import subprocess

import numpy as np

from .results import Results, same_reduce


def fmt_integer(contents, prefix="", postfix=""):
    return f"${prefix}{contents:d}{postfix}$"


def fmt_float(contents, prefix="", postfix=""):
    precision = max(0, 5 - len(f"{contents:.0f}"))
    precision = min(4, precision)
    return f"${prefix}{contents:.{precision}f}{postfix}$"


def fmt_scientific(contents, prefix="", postfix=""):
    mantissa, exponent = f"{contents:.1e}".split("e")
    return rf"${prefix}{mantissa}{{\cdot}}10^{{{exponent}}}{postfix}$"


def make_centered_cell(contents):
    return r"\multicolumn{1}{|c|}{" + contents + "}"


def make_merged_cell(contents, height):
    return r"\multirow{" + str(height) + r"}{*}{" + contents + "}"


def make_line(*contents):
    return r"&".join(contents) + r"\\" + "\n"


def make_header():
    return make_line(
        make_centered_cell(r"$N$"),
        make_centered_cell(r"$P$"),
        make_centered_cell(r"$T_{omp}$"),
        make_centered_cell(r"$S_{omp}$"),
        make_centered_cell(r"$T_{cuda}$"),
        make_centered_cell(r"$S_{cuda}$"),
        make_centered_cell(r"$T_{omp}/T_{cuda}$"),
        make_centered_cell(r"$\delta$"),
    )


def missing():
    return 5 * [make_centered_cell(r"\textbf{---}")]


def make_block(N, Ps, *stats):
    *stats, error = stats
    N_cell = make_merged_cell(fmt_integer(N, postfix="^3"), len(Ps))
    error_cell = make_merged_cell(fmt_scientific(error), len(Ps))

    block_lines = []
    for P, time, speedup, *extras in zip(Ps, *stats):
        P_cell = fmt_integer(P)
        time_cell = fmt_float(time, postfix="s")
        speedup_cell = fmt_float(speedup, postfix=r"\times")
        time_omp, speedup_omp = extras
        extra_cells = [
            fmt_float(time_omp, postfix="s"),
            fmt_float(speedup_omp, postfix=r"\times"),
            fmt_float(time / time_omp, postfix=r"\times"),
        ]

        block_lines.append(
            make_line(
                N_cell,
                P_cell,
                time_cell,
                speedup_cell,
                *extra_cells,
                error_cell,
            )
        )
        N_cell = ""
        error_cell = ""

    return (r"\cline{2-7}").join(block_lines)


def make_tabular(Ns, Ps, *stats):
    tabular_blocks = [make_header()]
    for N, *stat in zip(Ns, *stats):
        tabular_blocks.append(make_block(N, Ps, *stat))

    return (
        r"\begin{tabular}{|c|c|c|c|c|c|c|c|}"
        "\n"
        r"\hline" + r"\hline\hline".join(tabular_blocks) + r"\hline"
        "\n"
        r"\end{tabular}"
    )


def make_table(host, Ns, Ps, *stats):
    table = r"\caption{Ускорение (\texttt{CUDA vs OpenMP})}" "\n" + make_tabular(
        Ns, Ps, *stats
    )

    path = f"results/table.tex"
    with open(path, "w") as f:
        f.write(table)

    subprocess.check_output(["latexindent", f"--outputfile={path}", path])


def main():
    r = Results.load()
    rs = {
        "cpu": r["H":"cpu"],
        "gpu": r["H":"gpu"],
    }

    Ns = r["N"]
    Ps = r["P"]

    times = {}
    errors = {}
    for n, r in rs.items():
        times[n] = r.t(N=Ns, P=Ps)
        err = r.e(N=Ns, P=Ps)
        err = same_reduce(err, axes=(1, 2))
        err = err.max(axis=-1)
        errors[n] = err

    assert np.all(errors["cpu"] == errors["gpu"]), (errors["cpu"], errors["gpu"])
    errors = errors["gpu"]

    baselines = {n: np.min(times[n], axis=-1, keepdims=True) for n in times}
    speedups = {n: (baselines[n][:, :1] / times[n]).max(axis=-1) for n in rs}
    baselines = {n: baselines[n].squeeze(-1) for n in rs}

    make_table(
        "results",
        Ns,
        Ps,
        baselines["cpu"],
        speedups["cpu"],
        baselines["gpu"],
        speedups["gpu"],
        errors,
    )


if __name__ == "__main__":
    main()
