#!/bin/sh
set -euo pipefail
cd "$(dirname "$0")"

mkdir -p output
texfot pdflatex \
    -interaction=nonstopmode \
    -halt-on-error \
    -synctex=1 \
    -file-line-error \
    -output-directory=output \
    -shell-escape \
    $1 \
    && echo -ne "\n====>  Done!  <====\n" \
    || echo -ne "\n====> Failed! <====\n"
