#define mk_atomic_func(func)                                                    \
__device__ void func ## F(double *address, double value)                        \
{                                                                               \
    /* atomicMax works with unsigned integers, but we can still use it */       \
    /* because reinterpreting an IEEE float as an int keeps total ordering */   \
    func(                                                                       \
        (unsigned long long int *)address,                                      \
        *(unsigned long long int *)&value                                       \
    );                                                                          \
}

mk_atomic_func(atomicMax)
mk_atomic_func(atomicMax_block)
#undef mk_atomic_func

void *cudaCalloc(size_t num, size_t size)
{
    // cudaMalloc doesn't initialize the memory so we have to do it ourselves
    void *ret;
    cudaOK(cudaMalloc(&ret, num * size));
    cudaOK(cudaMemset(ret, 0, num * size));
    return ret;
}
