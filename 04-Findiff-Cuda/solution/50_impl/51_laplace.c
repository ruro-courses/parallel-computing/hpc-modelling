#define laplace_1d(sym, src, dst) do                                            \
    {                                                                           \
        const double inv_h2 = 1 / (                                             \
            _.nodes.steps.sym * _.nodes.steps.sym                               \
        );                                                                      \
                                                                                \
        /* for each inner element */                                            \
        kern_foreach(x, elm, _.block.shape)                                     \
            kern_foreach(y, elm, _.block.shape)                                 \
                kern_foreach(z, elm, _.block.shape)                             \
                {                                                               \
                    /* get previous, current and next elements */               \
                    vec3_int_t cur = get_index(elm);                            \
                    vec3_int_t prv = cur; prv.sym -= 1;                         \
                    vec3_int_t nxt = cur; nxt.sym += 1;                         \
                    double u_cur = src[s_(cur)];                                \
                    double u_prv = src[s_(prv)];                                \
                    double u_nxt = src[s_(nxt)];                                \
                                                                                \
                    /* compute the partial finite-difference */                 \
                    /* approximation for the laplace operator */                \
                    double u_dif = u_nxt - 2.0 * u_cur + u_prv;                 \
                    dst[s_(cur)] += inv_h2 * u_dif;                             \
                }                                                               \
    }                                                                           \
    while(false)

__global__ void _laplace_3d(const underscore_t _, const double * __restrict__ src, double * __restrict__ dst)
{
    // Accumulate the partial laplace along all 3 dimensions
    broadcast_vec3(laplace_1d, src, dst);
}
#undef laplace_1d

void laplace_3d(const double * __restrict__ src, double * __restrict__ dst)
{
    _laplace_3d<<<kern_spec(__.block.shape)>>>(__,  src, dst);
    cudaOK(cudaGetLastError());
}
