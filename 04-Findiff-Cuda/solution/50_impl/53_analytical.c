// np.sin(A * p + B).prod(axis=-1)
// Also, instead of cos(a_t * t + b_t), we compute sin(... + pi/2)
#define formula(sym, pos)                                                       \
    sin(_.param.a.sym * pos.sym + _.param.b.sym)

#define with_analytical(callback, step, ...) do                                 \
    {                                                                           \
        /* for each inner element */                                            \
        kern_foreach(x, elm, _.block.shape)                                     \
            kern_foreach(y, elm, _.block.shape)                                 \
                kern_foreach(z, elm, _.block.shape)                             \
                {                                                               \
                    /* get the element index and floating coordinates */        \
                    vec3_int_t cur = get_index(elm);                            \
                    vec4_f64_t pos = get_coord(cur, step);                      \
                                                                                \
                    /* compute the analytical values of the function */         \
                    double val = _broadcast_vec4((*), formula, pos);            \
                                                                                \
                    /* run the callback with the computed value */              \
                    callback(cur, val, ##__VA_ARGS__);                          \
                }                                                               \
    }                                                                           \
    while(false)

__global__ void _set_analytical(const underscore_t _, int step, double *dst)
{
    // Compute the analytical solution and store it into dst
#define set_value(cur, val, dst)                                                \
    dst[s_(cur)] = val
    with_analytical(set_value, step, dst);
#undef set_value
}

void set_analytical(int step, double *dst)
{
    _set_analytical<<<kern_spec(__.block.shape)>>>(__, step, dst);
    cudaOK(cudaGetLastError());
}

__device__ double d_result = 0.0;
__global__ void _max_analytical_error(const underscore_t _, int step, const double * ref)
{
    // Initialize threadblock-local (shared) memory
    __shared__ double block_err;
    if (threadIdx.x == 0 && threadIdx.y == 0 && threadIdx.z == 0)
        block_err = 0.0;
    __syncthreads();

    // Initialize threadlocal memory
    double err = 0.0;

    // Compute analytical solution. Use it to find the maximum error in ref.
    // The analytical solution isn't stored, no extra memory is used.
#define update_error(cur, val, ref, err)                                        \
    err = fmax(err, fabs(val - ref[s_(cur)]))
    with_analytical(update_error, step, ref, err);
#undef update_error

    // Atomically aggregate the values in each thread block
    atomicMax_blockF(&block_err, err);
    __syncthreads();

    // Atomically aggregate the values between all blocks
    if (threadIdx.x == 0 && threadIdx.y == 0 && threadIdx.z == 0)
        atomicMaxF(&d_result, block_err);
}

double max_analytical_error(int step, const double * ref)
{
    // Init result variable
    double h_result = 0.0;
    cudaOK(cudaMemcpyToSymbol(d_result, &h_result, sizeof(double)));

    // Run kernel
    _max_analytical_error<<<kern_spec(__.block.shape)>>>(__, step, ref);
    cudaOK(cudaGetLastError());

    // Get result
    cudaOK(cudaMemcpyFromSymbol(&h_result, d_result, sizeof(double)));
    return h_result;
}

#undef formula
#undef with_analytical
