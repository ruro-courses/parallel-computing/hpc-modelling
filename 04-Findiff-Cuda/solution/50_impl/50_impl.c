#include "51_laplace.c"
#include "52_edges.c"
#include "53_analytical.c"
#include "54_step.c"
#include "55_cuda.c"

void run(void)
{
    // Start the timer
    MPI_Barrier(__.local.comm);
    double tstart = MPI_Wtime();

    // Run for K timesteps
    double errors[__.nodes.shape.t + 1];
    for (int t = 0; t <= __.nodes.shape.t; ++t)
    {
        double local_error = step(t);

        // Reduce the error over all processes
        MPI_Allreduce(&local_error, &errors[t], 1, MPI_DOUBLE, MPI_MAX, __.local.comm);
    }

    // Stop the timer
    MPI_Barrier(__.local.comm);
    double time = MPI_Wtime() - tstart;

    // Report results
    root_printf(__.local, "__TRIALS__.append((%le, [", time);
    for (int t = 0; t <= __.nodes.shape.t; ++t)
        root_printf(__.local, "%le, ", errors[t]);
    root_printf(__.local, "]))\n");
}
