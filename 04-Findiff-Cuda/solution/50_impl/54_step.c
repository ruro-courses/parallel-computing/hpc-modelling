void step0(void)
{
    // Initialize the first layer with the analytical solution
    set_analytical(0, __.block.array.m[2]);
}

__global__ void _step1(const underscore_t _)
{
    double t2_2 = (_.nodes.steps.t * _.nodes.steps.t) / 2.0;

    // for each inner element
    kern_foreach(x, elm, _.block.shape)
        kern_foreach(y, elm, _.block.shape)
            kern_foreach(z, elm, _.block.shape)
            {
                vec3_int_t cur = get_index(elm);

                // Get the value from previous steps and the current laplace value
                double u0 = _.block.array.m[1][s_(cur)];
                double lp = _.block.array.m[2][s_(cur)];

                // Compute the next value
                _.block.array.m[2][s_(cur)] = t2_2 * lp + u0;
            }
}

void step1(void)
{
    // Compute the laplace operator
    laplace_3d(__.block.array.m[1], __.block.array.m[2]);

    // Compute the first step
    _step1<<<kern_spec(__.block.shape)>>>(__);
    cudaOK(cudaGetLastError());
}

__global__ void _stepN(const underscore_t _)
{
    double t2 = _.nodes.steps.t * _.nodes.steps.t;

    // for each inner element
    kern_foreach(x, elm, _.block.shape)
        kern_foreach(y, elm, _.block.shape)
            kern_foreach(z, elm, _.block.shape)
            {
                vec3_int_t cur = get_index(elm);

                // Get the values from 2 previous steps and the current laplace value
                double u0 = _.block.array.m[0][s_(cur)];
                double u1 = _.block.array.m[1][s_(cur)];
                double lp = _.block.array.m[2][s_(cur)];

                // Compute the next value
                _.block.array.m[2][s_(cur)] = t2 * lp + 2.0 * u1 - u0;
            }
}

void stepN(void)
{
    // Compute the laplace operator
    laplace_3d(__.block.array.m[1], __.block.array.m[2]);

    // Compute the Nth step
    _stepN<<<kern_spec(__.block.shape)>>>(__);
    cudaOK(cudaGetLastError());
}

double step(int num)
{
    // Rotate buffers
    free_block_array(__.block.array.m[0]);
    __.block.array.m[0] = __.block.array.m[1];
    __.block.array.m[1] = __.block.array.m[2];
    __.block.array.m[2] = alloc_block_array();

    // Do the timestep
    if (num == 0) step0();
    else if (num == 1) step1();
    else stepN();

    // Exchange and update the edge values
    edges_3d(__.block.array.m[2]);

    // Compute the maximum error
    return max_analytical_error(num, __.block.array.m[2]);
}
