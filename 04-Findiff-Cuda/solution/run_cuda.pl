#!/bin/sh
PROCS="${1}"
if [ "${PROCS}" == 1 ];
then
    GPUS_PER_HOST=1
else
    GPUS_PER_HOST=2
fi

shift

bsub <<EOF
source /polusfs/setenv/setup.OpenMPI
#BSUB -n ${PROCS}
#BSUB -W 00:15
#BSUB -gpu "num=${GPUS_PER_HOST}:mode=exclusive_process"
#BSUB -R "span[ptile=${GPUS_PER_HOST}]"
#BSUB -o main.%J.out
#BSUB -e main.%J.err
OMP_NUM_THREADS=1 mpiexec ${@}
EOF
