// MPI cartesian communicator
typedef struct
{
    MPI_Comm comm;
    int rank;
    vec3_int_t shape; // Px Py Pz
    vec3_int_t coord; // px py pz
} local_t;

// Debug print most of the local state information
#define dump_local(local) do                                                    \
    {                                                                           \
        dump_field(local.rank, "d");                                            \
        dump_vecN(3, local.shape, "d");                                         \
        dump_vecN(3, local.coord, "d");                                         \
    }                                                                           \
    while(false)
