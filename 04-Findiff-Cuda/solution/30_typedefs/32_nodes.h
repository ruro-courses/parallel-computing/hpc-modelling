// Global node grid
typedef struct
{
    vec4_int_t shape; // Nx Ny Nz K
    vec4_f64_t bound; // Lx Ly Lz T
    vec4_f64_t steps; // hx hy hz tau
} nodes_t;

// Debug print most of the nodes state information
#define dump_nodes(nodes) do                                                    \
    {                                                                           \
        dump_vecN(4, nodes.shape, "d");                                         \
        dump_vecN(4, nodes.bound, "lf");                                        \
        dump_vecN(4, nodes.steps, "lf");                                        \
    }                                                                           \
    while(false)
