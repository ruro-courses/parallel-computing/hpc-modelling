// MPI world communicator
typedef struct
{
    MPI_Comm comm;
    int size;
    int rank;
} world_t;

// Debug print most of the world state information
#define dump_world(world) do                                                    \
    {                                                                           \
        dump_field(world.size, "d");                                            \
        dump_field(world.rank, "d");                                            \
    }                                                                           \
    while(false)
