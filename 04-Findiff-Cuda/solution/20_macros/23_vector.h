// Declare a field with specified name and type
#define make_field(name, type) type name

// Print the name of the field and its value
#define dump_field(field, fmt)                                                  \
    printf(str(field) " = %" fmt ";\n", field)

// Declare a vector of N elements of specified type
#define make_vecN_t(N, name, type)                                              \
    typedef union                                                               \
    {                                                                           \
        struct { broadcast_vec ## N (make_field, type); };                      \
        type m[N];                                                              \
    } vec ## N ## _ ## name ##  _t

// Vector formatting options
#define fmt_sym(sym, fmt) "." str(sym) "=%" fmt
#define fmt_arr(sym, fmt) "%" fmt

// Pretty-print a vector field and its values
#define _dump_vecN(N, field, fmt, fmt_fun, ll, rr) do                           \
    {                                                                           \
        printf(                                                                 \
            str(field) " = " ll                                                 \
            _broadcast_vec ## N ((", "), fmt_fun, fmt)                          \
            rr ";\n",                                                           \
            _broadcast_vec ## N ((,), get_field, field)                         \
        );                                                                      \
    }                                                                           \
    while(false)

// Pretty-print a named vector field and its values
#define dump_vecN(N, field, fmt)                                                \
    _dump_vecN(N, field, fmt, fmt_sym, "{", "}")

// Pretty-print an array field and its values
#define dump_arrN(N, field, fmt)                                                \
    _dump_vecN(N, field, fmt, fmt_arr, "[", "]")
