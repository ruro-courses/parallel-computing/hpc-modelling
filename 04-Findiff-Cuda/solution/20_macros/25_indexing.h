// Get the flat index of the element specified by the 3d index idx
#define s_(idx)                                                                 \
    (                                                                           \
        idx.x * _.block.shape.y * _.block.shape.z +                             \
        idx.y                   * _.block.shape.z +                             \
        idx.z                                                                   \
    )

// Iterate over each index in block along axis sym
#define foreach(sym, elm, shape)                                                \
    for (                                                                       \
        int elm ## sym = 1;                                                     \
        elm ## sym < shape.sym - 1;                                             \
        ++elm ## sym                                                            \
    )

// Construct the index struct based on element indices (from foreach)
#define get_index(elm)                                                          \
    (vec3_int_t) {{elm ## x, elm ## y, elm ## z}}

// Get the floating point coordinate of the point (along axis)
#define _get_coord(sym, idx)                                                    \
    _.nodes.bound.sym * (idx.sym - 1 + _.block.coord.sym) / _.nodes.shape.sym

// Get the floating point coordinate of the point (3d)
#define get_coord(idx, step)                                                    \
    (vec4_f64_t)                                                                \
    {{                                                                          \
        _broadcast_vec3((,), _get_coord, idx),                                  \
        _.nodes.bound.t * step / _.nodes.shape.t                                \
    }}

// Pretty-print the buffer contents (suitable for numpy ingestion)
#define dump_buffer(buffer)                                                     \
    foreach(x, elm, _.block.shape)                                              \
        foreach(y, elm, _.block.shape)                                          \
            foreach(z, elm, _.block.shape)                                      \
            {                                                                   \
                vec3_int_t cur = get_index(elm);                                \
                printf(                                                         \
                    "arr[%d, %d, %d] = %le\n",                                  \
                    _.block.coord.x + cur.x - 1,                                \
                    _.block.coord.y + cur.y - 1,                                \
                    _.block.coord.z + cur.z - 1,                                \
                    buffer[s_(cur)]                                             \
                );                                                              \
            }
