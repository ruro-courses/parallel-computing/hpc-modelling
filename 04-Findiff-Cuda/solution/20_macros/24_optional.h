// CUDA-specific includes and macros
#include <cuda.h>
#include <cuda_runtime_api.h>

#if !(defined(MPIX_CUDA_AWARE_SUPPORT) && MPIX_CUDA_AWARE_SUPPORT)
    #error "Your MPI version doesn't have CUDA-aware support."
#endif

// Check that the cuda action succeeded, print an error if not
#define cudaOK(action) do                                                       \
    {                                                                           \
        cudaError_t result = action;                                            \
        if (result != cudaSuccess)                                              \
        {                                                                       \
             printf(                                                            \
                 "%s:%d: %s\n",                                                 \
                 __FILE__, __LINE__,                                            \
                 cudaGetErrorString(result)                                     \
             );                                                                 \
             stop(1, "CUDA ERROR.");                                            \
        }                                                                       \
    }                                                                           \
    while(false)

// Compute the kernel offset of the current thread for kern_foreach
#define kern_offset(sym, off, shape)                                            \
    (                                                                           \
        1 +                                                                     \
        (                                                                       \
            (                                                                   \
                off +                                                           \
                threadIdx.sym +                                                 \
                blockIdx.sym * blockDim.sym                                     \
            ) *                                                                 \
            (                                                                   \
                shape.sym - 2                                                   \
            )                                                                   \
        ) /                                                                     \
        (                                                                       \
            blockDim.sym *                                                      \
            gridDim.sym                                                         \
        )                                                                       \
    )

// Iterate over each index in block along axis sym (cuda kernel version)
#define kern_foreach(sym, elm, shape)                                           \
    for (                                                                       \
        int elm ## sym = kern_offset(sym, 0, shape);                            \
        elm ## sym < kern_offset(sym, 1, shape);                                \
        ++elm ## sym                                                            \
    )

// Specify the preferred grid and block dimensions for cuda kernels
#define kern_spec(sh)                                                           \
    dim3(                                                                       \
        (sh.x - 2),                                                             \
        (sh.y - 2),                                                             \
        1                                                                       \
    ),                                                                          \
    dim3(                                                                       \
        1,                                                                      \
        1,                                                                      \
        min(64, sh.z - 2)                                                       \
    )

// Atomically update the value at address to the maximum value
#define mk_atomic_func(func)                                                    \
__device__ void func ## F(double *address, double value);
mk_atomic_func(atomicMax)
mk_atomic_func(atomicMax_block)
#undef mk_atomic_func

// Allocate a zero-initialized chunk of memory
void *cudaCalloc(size_t num, size_t size);
