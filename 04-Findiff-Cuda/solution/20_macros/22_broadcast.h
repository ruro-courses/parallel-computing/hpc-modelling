// Broadcasting with arbitrary separator
#define _broadcast_vec3_full(sep, apply, ...)                                   \
    apply(x, y, z, ##__VA_ARGS__) unparen sep                                   \
    apply(y, x, z, ##__VA_ARGS__) unparen sep                                   \
    apply(z, x, y, ##__VA_ARGS__)

#define _broadcast_vec3(sep, apply, ...)                                        \
    apply(x, ##__VA_ARGS__) unparen sep                                         \
    apply(y, ##__VA_ARGS__) unparen sep                                         \
    apply(z, ##__VA_ARGS__)

#define _broadcast_vec4(sep, apply, ...)                                        \
    _broadcast_vec3(sep, apply, ##__VA_ARGS__) unparen sep                      \
    apply(t, ##__VA_ARGS__)

// Broadcasting with semicolon
#define broadcast_vec3_full(apply, ...)                                         \
    _broadcast_vec3_full((;), apply, ##__VA_ARGS__)

#define broadcast_vec3(apply, ...)                                              \
    _broadcast_vec3((;), apply, ##__VA_ARGS__)

#define broadcast_vec4(apply, ...)                                              \
    _broadcast_vec4((;), apply, ##__VA_ARGS__)

// Helpful broadcastable functions
#define get_field(sym, field)                                                   \
    field.sym

#define set_field_val(sym, field, value)                                        \
    field.sym = value

#define set_field_fun(sym, field, funct, ...)                                   \
    field.sym = funct(sym, ##__VA_ARGS__)

#define call_field_fun(sym, field, funct, ...)                                  \
    funct(field.sym, ##__VA_ARGS__)
