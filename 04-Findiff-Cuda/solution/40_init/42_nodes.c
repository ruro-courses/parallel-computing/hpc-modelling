void init_nodes(double L, double T, int N, int K)
{
    // shape = N, N, N, K
    broadcast_vec3(set_field_val, __.nodes.shape, N);
    __.nodes.shape.t = K;

    // bound = L, L, L, T
    broadcast_vec3(set_field_val, __.nodes.bound, L);
    __.nodes.bound.t = T;

    // steps = L/N, L/N, L/N, T/K
#define init_steps(sym)                                                         \
    __.nodes.bound.sym / __.nodes.shape.sym
    broadcast_vec4(set_field_fun, __.nodes.steps, init_steps);
#undef init_steps
}

