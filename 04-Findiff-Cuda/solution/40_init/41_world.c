// Get environment variable by name and parse it as an int
int getenv_int(const char *name)
{
    char *envvar = getenv(name);
    return envvar ? atoi(envvar) : -1;
}

void init_world(void)
{
    // Get world communicator rank and size
    __.world.comm = MPI_COMM_WORLD;
    MPI_Comm_size(__.world.comm, &__.world.size);
    MPI_Comm_rank(__.world.comm, &__.world.rank);

    // Get local rank and size (for each host)
    int loc_rank = getenv_int("OMPI_COMM_WORLD_LOCAL_RANK");
    int loc_size = getenv_int("OMPI_COMM_WORLD_LOCAL_SIZE");
    if ((loc_size < 0) || (loc_rank < 0))
        stop(2, "Couldn't get the local process rank and size from env.");

    // Distribute the available GPUs among local processes
    cudaSetDevice(loc_rank);

    // Get local GPU info
    int gpu_size = 0;
    int gpu_rank = 0;
    char device_pci[16];

    cudaGetDeviceCount(&gpu_size);
    cudaGetDevice(&gpu_rank);
    cudaDeviceGetPCIBusId(device_pci, 15, gpu_rank);

    // Print allocated GPU information
    printf(
        "WORLD[%d/%d] | LOCAL[%d/%d] | CUDA[%d/%d] | %s\n",
        __.world.rank, __.world.size,
        loc_rank, loc_size,
        gpu_rank, gpu_size,
        device_pci
    );
    fflush(stdout);
}
