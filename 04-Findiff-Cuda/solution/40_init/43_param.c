void init_param(void)
{
    __.param.period = (vec3_int_t)
    {{
        // x axis uses periodic boundary condition
        true,
        // y and z axes use first-type boundary condition
        false,
        false,
    }};

    // Multiplicative coefficients
    __.param.a = (vec4_f64_t)
    {{
        2.0 * M_PI / __.nodes.bound.x,
        M_PI / __.nodes.bound.y,
        M_PI / __.nodes.bound.z,
        M_PI * sqrt(
            4.0 / (__.nodes.bound.x * __.nodes.bound.x) +
            1.0 / (__.nodes.bound.y * __.nodes.bound.y) +
            1.0 / (__.nodes.bound.z * __.nodes.bound.z)
        ),
    }};

    // Additive coefficients
    __.param.b = (vec4_f64_t)
    {{
        0.0,
        0.0,
        0.0,
        2.0 * M_PI,
    }};

    // Change time term to use cosine instead of sine
    __.param.b.t += M_PI / 2;
}
