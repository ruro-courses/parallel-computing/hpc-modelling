void init_local(void)
{
    // Don't split along the z axis. Splitting blocks along the z axis makes
    // MPI+CUDA perform O(N^2) individual HtoD/DtoH copies for each Sendrecv
    // See [this issue](https://github.com/open-mpi/ompi/issues/8720).
    __.local.shape.z = 1;
    MPI_Dims_create(__.world.size, 3, __.local.shape.m);

    // Initialize the cartesian communicator
    MPI_Cart_create(__.world.comm, 3, __.local.shape.m, __.param.period.m, true, &__.local.comm);
    MPI_Comm_rank(__.local.comm, &__.local.rank);
    MPI_Cart_coords(__.local.comm, __.local.rank, 3, __.local.coord.m);
}

void fini_local(void)
{
    MPI_Comm_free(&__.local.comm);
}
