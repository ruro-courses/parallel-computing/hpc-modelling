// The underscore is the global state object.
// See 30_typedefs/30_typedefs.h for more info.
underscore_t __;

#include "41_world.c"
#include "42_nodes.c"
#include "43_param.c"
#include "44_local.c"
#include "45_block.c"

void init_underscore(int argc, char *argv[])
{
    // Init MPI and world communicator
    MPI_Init(&argc, &argv);
    init_world();

    // Parse arguments
    if (argc != 5) stop(1, "Usage: %s [L] [T] [N] [K]\n", argv[0]);

    double L, T;
    int N, K;
    if (sscanf(argv[1], "%lf", &L) != 1) stop(1, "Couldn't parse [L] as a double.");
    if (sscanf(argv[2], "%lf", &T) != 1) stop(1, "Couldn't parse [T] as a double.");
    if (sscanf(argv[3], "%d", &N) != 1) stop(1, "Couldn't parse [N] as an integer.");
    if (sscanf(argv[4], "%d", &K) != 1) stop(1, "Couldn't parse [K] as an integer.");

    // Report the current hyperparameters
    root_printf(
        __.world,
        "__PARAMS__ = {'P': %d, 'O': %d, 'L': %le, 'T': %le, 'N': %d, 'K': %d}\n",
        __.world.size, 0, L, T, N, K
    );

    // Init everything else
    init_nodes(L, T, N, K);
    init_param();
    init_local();
    init_block();
}

void fini_underscore(void)
{
    fini_block();
    fini_local();
    MPI_Finalize();
}
