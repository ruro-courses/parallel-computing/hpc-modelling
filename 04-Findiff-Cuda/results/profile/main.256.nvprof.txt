======== Profiling result:
            Type  Time(%)      Time     Calls       Avg       Min       Max  Name
 GPU activities:   69.81%  319.89ms        21  15.233ms  15.190ms  15.727ms  _max_analytical_error(underscore_t, int, double const *)
                   12.17%  55.760ms        20  2.7880ms  2.7833ms  2.7986ms  _laplace_3d(underscore_t, double const *, double*)
                   10.22%  46.825ms        19  2.4645ms  2.4194ms  2.5174ms  _stepN(underscore_t)
                    3.40%  15.585ms         1  15.585ms  15.585ms  15.585ms  _set_analytical(underscore_t, int, double*)
                    2.63%  12.049ms        24  502.05us  430.34us  513.09us  [CUDA memset]
                    0.55%  2.5089ms      1407  1.7830us     672ns  2.5600us  [CUDA memcpy DtoH]
                    0.44%  1.9964ms         1  1.9964ms  1.9964ms  1.9964ms  _step1(underscore_t)
                    0.43%  1.9808ms        21  94.323us  94.017us  94.945us  _condition_3d(underscore_t, double*)
                    0.36%  1.6457ms      1407  1.1690us     544ns  1.5680us  [CUDA memcpy HtoD]
      API calls:   54.95%  320.09ms        21  15.243ms  15.197ms  15.738ms  cudaMemcpyFromSymbol
                   24.83%  144.64ms      2772  52.179us  2.0990us  15.051ms  cuMemcpyAsync
                   17.46%  101.74ms        24  4.2390ms  151.10us  79.379ms  cudaMalloc
                    1.05%  6.1263ms      2772  2.2100us     414ns  17.880us  cuStreamSynchronize
                    1.03%  5.9831ms        24  249.30us  135.38us  1.0820ms  cudaFree
                    0.35%  2.0306ms        21  96.696us  96.022us  97.475us  cudaMemcpyToSymbol
                    0.11%  657.07us         1  657.07us  657.07us  657.07us  cuMemHostRegister
                    0.06%  346.92us        83  4.1790us  2.5750us  8.2450us  cudaLaunchKernel
                    0.04%  245.08us      1200     204ns     190ns     891ns  cuEventCreate
                    0.04%  212.99us      1200     177ns     129ns  9.8120us  cuEventDestroy
                    0.03%  202.05us        24  8.4180us  7.0630us  15.416us  cudaMemset
                    0.02%  93.098us       101     921ns     101ns  37.886us  cuDeviceGetAttribute
                    0.01%  77.561us       214     362ns     125ns  3.4420us  cuPointerGetAttributes
                    0.01%  52.091us         1  52.091us  52.091us  52.091us  cuMemHostUnregister
                    0.00%  14.589us         1  14.589us  14.589us  14.589us  cuDeviceGetName
                    0.00%  12.209us        83     147ns     102ns     263ns  cudaGetLastError
                    0.00%  11.941us         4  2.9850us  1.1050us  7.9350us  cuStreamCreate
                    0.00%  11.384us        85     133ns      93ns     234ns  cuCtxGetCurrent
                    0.00%  9.2760us         4  2.3190us  1.3590us  4.9180us  cuStreamDestroy
                    0.00%  3.8800us         1  3.8800us  3.8800us  3.8800us  cuDeviceGetPCIBusId
                    0.00%  2.1140us         1  2.1140us  2.1140us  2.1140us  cudaSetDevice
                    0.00%  1.6380us         1  1.6380us  1.6380us  1.6380us  cudaDeviceGetPCIBusId
                    0.00%  1.1740us         1  1.1740us  1.1740us  1.1740us  cudaGetDevice
                    0.00%  1.0130us         3     337ns     159ns     581ns  cuDeviceGetCount
                    0.00%     743ns         2     371ns     140ns     603ns  cuDeviceGet
                    0.00%     300ns         1     300ns     300ns     300ns  cuDeviceTotalMem
                    0.00%     281ns         1     281ns     281ns     281ns  cudaGetDeviceCount
                    0.00%     185ns         1     185ns     185ns     185ns  cuDeviceGetUuid

======== NVTX result:
========   Thread "MPI Rank 0" (id = 360640512)
========     Domain "<unnamed>"
========       Range "MPI_Allreduce"
            Type  Time(%)      Time     Calls       Avg       Min       Max  Name
          Range:  100.00%  75.539us        21  3.5970us  3.0610us  5.5900us  MPI_Allreduce
No kernels were profiled in this range.
No API activities were profiled in this range.

========       Range "MPI_Barrier"
            Type  Time(%)      Time     Calls       Avg       Min       Max  Name
          Range:  100.00%  2.3430us         4     585ns     231ns  1.6340us  MPI_Barrier
No kernels were profiled in this range.
No API activities were profiled in this range.

========       Range "MPI_Sendrecv"
            Type  Time(%)      Time     Calls       Avg       Min       Max  Name
          Range:  100.00%  152.80ms       126  1.2127ms     123ns  16.645ms  MPI_Sendrecv
 GPU activities:   60.48%  2.4943ms      1386  1.7990us  1.2160us  2.5600us  [CUDA memcpy DtoH]
                   39.52%  1.6296ms      1386  1.1750us     863ns  1.5680us  [CUDA memcpy HtoD]
      API calls:  100.00%  144.64ms      2772  52.179us  2.0990us  15.051ms  cuMemcpyAsync

