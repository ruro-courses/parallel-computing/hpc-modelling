  _stepN(underscore_t), 2021-Dec-25 04:54:18, Context 1, Stream 7
    Section: GPU Speed Of Light Throughput
    ---------------------------------------------------------------------- --------------- ------------------------------
    DRAM Frequency                                                           cycle/nsecond                           6.75
    SM Frequency                                                             cycle/nsecond                           1.33
    Elapsed Cycles                                                                   cycle                        317,628
    Memory [%]                                                                           %                          88.13
    DRAM Throughput                                                                      %                          88.13
    Duration                                                                       usecond                         236.70
    L1/TEX Cache Throughput                                                              %                          39.67
    L2 Cache Throughput                                                                  %                          52.70
    SM Active Cycles                                                                 cycle                     312,054.53
    Compute (SM) [%]                                                                     %                          38.76
    ---------------------------------------------------------------------- --------------- ------------------------------
    INF   The kernel is utilizing greater than 80.0% of the available compute or memory performance of the device. To   
          further improve performance, work will likely need to be shifted from the most utilized to another unit.      
          Start by analyzing workloads in the Memory Workload Analysis section.                                         

    Section: Compute Workload Analysis
    ---------------------------------------------------------------------- --------------- ------------------------------
    Executed Ipc Active                                                         inst/cycle                           0.65
    Executed Ipc Elapsed                                                        inst/cycle                           0.64
    Issue Slots Busy                                                                     %                          16.20
    Issued Ipc Active                                                           inst/cycle                           0.65
    SM Busy                                                                              %                          39.20
    ---------------------------------------------------------------------- --------------- ------------------------------
          No pipeline is over-utilized.                                                                                 

    Section: Memory Workload Analysis
    ---------------------------------------------------------------------- --------------- ------------------------------
    Memory Throughput                                                         Gbyte/second                         285.44
    Mem Busy                                                                             %                          52.70
    Max Bandwidth                                                                        %                          88.13
    L1/TEX Hit Rate                                                                      %                          25.07
    L2 Hit Rate                                                                          %                          59.53
    Mem Pipes Busy                                                                       %                          10.38
