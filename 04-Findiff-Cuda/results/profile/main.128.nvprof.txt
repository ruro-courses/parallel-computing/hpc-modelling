======== Profiling result:
            Type  Time(%)      Time     Calls       Avg       Min       Max  Name
 GPU activities:   67.46%  44.475ms        21  2.1179ms  2.1073ms  2.2269ms  _max_analytical_error(underscore_t, int, double const *)
                   17.54%  11.566ms        20  578.28us  576.74us  578.79us  _laplace_3d(underscore_t, double const *, double*)
                    6.94%  4.5742ms        19  240.74us  239.39us  244.45us  _stepN(underscore_t)
                    3.35%  2.2073ms         1  2.2073ms  2.2073ms  2.2073ms  _set_analytical(underscore_t, int, double*)
                    1.96%  1.2912ms        24  53.798us  53.089us  56.737us  [CUDA memset]
                    1.01%  666.61us       399  1.6700us     672ns  2.4960us  [CUDA memcpy DtoH]
                    0.81%  531.24us        21  25.297us  25.185us  25.408us  _condition_3d(underscore_t, double*)
                    0.67%  439.97us       399  1.1020us     544ns  1.6970us  [CUDA memcpy HtoD]
                    0.27%  178.21us         1  178.21us  178.21us  178.21us  _step1(underscore_t)
      API calls:   53.56%  84.273ms        24  3.5114ms  70.155us  76.818ms  cudaMalloc
                   28.40%  44.683ms        21  2.1278ms  2.1166ms  2.2379ms  cudaMemcpyFromSymbol
                   14.69%  23.116ms       756  30.576us  1.8280us  1.7899ms  cuMemcpyAsync
                    1.06%  1.6625ms       756  2.1990us     411ns  6.9500us  cuStreamSynchronize
                    1.06%  1.6604ms        24  69.184us  49.971us  80.283us  cudaFree
                    0.37%  584.28us        21  27.823us  27.341us  28.062us  cudaMemcpyToSymbol
                    0.21%  337.32us        83  4.0640us  2.5560us  8.4140us  cudaLaunchKernel
                    0.16%  251.73us      1200     209ns     192ns     820ns  cuEventCreate
                    0.13%  209.82us      1200     174ns     147ns  10.155us  cuEventDestroy
                    0.13%  205.21us        24  8.5500us  6.7970us  18.890us  cudaMemset
                    0.06%  98.899us       101     979ns     103ns  41.647us  cuDeviceGetAttribute
                    0.05%  71.498us       214     334ns     134ns  1.5460us  cuPointerGetAttributes
                    0.04%  69.149us         1  69.149us  69.149us  69.149us  cuMemHostRegister
                    0.03%  49.932us         1  49.932us  49.932us  49.932us  cuMemHostUnregister
                    0.01%  15.680us         1  15.680us  15.680us  15.680us  cuDeviceGetName
                    0.01%  12.354us         4  3.0880us  1.2870us  8.1590us  cuStreamCreate
                    0.01%  11.768us         4  2.9420us  1.3020us  7.5410us  cuStreamDestroy
                    0.01%  11.599us        83     139ns     102ns     241ns  cudaGetLastError
                    0.01%  10.343us        85     121ns      94ns     195ns  cuCtxGetCurrent
                    0.00%  4.8970us         1  4.8970us  4.8970us  4.8970us  cuDeviceGetPCIBusId
                    0.00%  2.3220us         1  2.3220us  2.3220us  2.3220us  cudaSetDevice
                    0.00%  1.5310us         1  1.5310us  1.5310us  1.5310us  cudaDeviceGetPCIBusId
                    0.00%  1.1730us         3     391ns     145ns     629ns  cuDeviceGetCount
                    0.00%  1.0200us         1  1.0200us  1.0200us  1.0200us  cudaGetDevice
                    0.00%     769ns         2     384ns     173ns     596ns  cuDeviceGet
                    0.00%     515ns         1     515ns     515ns     515ns  cudaGetDeviceCount
                    0.00%     376ns         1     376ns     376ns     376ns  cuDeviceTotalMem
                    0.00%     189ns         1     189ns     189ns     189ns  cuDeviceGetUuid

======== NVTX result:
========   Thread "MPI Rank 0" (id = 3065724928)
========     Domain "<unnamed>"
========       Range "MPI_Allreduce"
            Type  Time(%)      Time     Calls       Avg       Min       Max  Name
          Range:  100.00%  69.180us        21  3.2940us  2.9140us  5.0190us  MPI_Allreduce
No kernels were profiled in this range.
No API activities were profiled in this range.

========       Range "MPI_Barrier"
            Type  Time(%)      Time     Calls       Avg       Min       Max  Name
          Range:  100.00%  2.2710us         4     567ns     246ns  1.4740us  MPI_Barrier
No kernels were profiled in this range.
No API activities were profiled in this range.

========       Range "MPI_Sendrecv"
            Type  Time(%)      Time     Calls       Avg       Min       Max  Name
          Range:  100.00%  25.650ms       126  203.57us     124ns  2.4112ms  MPI_Sendrecv
 GPU activities:   60.38%  651.54us       378  1.7230us     863ns  2.4960us  [CUDA memcpy DtoH]
                   39.62%  427.46us       378  1.1300us     704ns  1.6970us  [CUDA memcpy HtoD]
      API calls:  100.00%  23.116ms       756  30.576us  1.8280us  1.7899ms  cuMemcpyAsync

