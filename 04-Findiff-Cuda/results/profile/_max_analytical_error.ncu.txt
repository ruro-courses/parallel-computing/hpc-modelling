  _max_analytical_error(underscore_t, int, const double *), 2021-Dec-25 04:53:32, Context 1, Stream 7
    Section: GPU Speed Of Light Throughput
    ---------------------------------------------------------------------- --------------- ------------------------------
    DRAM Frequency                                                           cycle/nsecond                           6.79
    SM Frequency                                                             cycle/nsecond                           1.35
    Elapsed Cycles                                                                   cycle                      4,281,865
    Memory [%]                                                                           %                           3.44
    DRAM Throughput                                                                      %                           1.67
    Duration                                                                       msecond                           3.15
    L1/TEX Cache Throughput                                                              %                           6.50
    L2 Cache Throughput                                                                  %                           1.66
    SM Active Cycles                                                                 cycle                   4,242,264.40
    Compute (SM) [%]                                                                     %                          87.38
    ---------------------------------------------------------------------- --------------- ------------------------------
    INF   The kernel is utilizing greater than 80.0% of the available compute or memory performance of the device. To   
          further improve performance, work will likely need to be shifted from the most utilized to another unit.      
          Start by analyzing workloads in the Compute Workload Analysis section.                                        

    Section: Compute Workload Analysis
    ---------------------------------------------------------------------- --------------- ------------------------------
    Executed Ipc Active                                                         inst/cycle                           0.19
    Executed Ipc Elapsed                                                        inst/cycle                           0.19
    Issue Slots Busy                                                                     %                           4.77
    Issued Ipc Active                                                           inst/cycle                           0.19
    SM Busy                                                                              %                          87.61
    ---------------------------------------------------------------------- --------------- ------------------------------
    WRN   FP64 is the highest-utilized pipeline (87.6%). It executes 64-bit floating point operations. The pipeline is  
          over-utilized and likely a performance bottleneck. See the Kernel Profiling Guide                             
          (https://docs.nvidia.com/nsight-compute/ProfilingGuide/index.html#metrics-decoder) for the workloads handled  
          by each pipeline.                                                                                             

    Section: Memory Workload Analysis
    ---------------------------------------------------------------------- --------------- ------------------------------
    Memory Throughput                                                         Gbyte/second                           5.44
    Mem Busy                                                                             %                           3.25
    Max Bandwidth                                                                        %                           3.44
    L1/TEX Hit Rate                                                                      %                          86.02
    L2 Hit Rate                                                                          %                          68.34
    Mem Pipes Busy                                                                       %                          22.07
