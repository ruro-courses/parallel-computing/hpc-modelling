class Node:
    def __init__(self, dependencies):
        depth = max((1 + d.depth for d in dependencies), default=0)
        self.dependencies = dependencies
        self.depth = depth


def make_data(n, m):
    data = {}
    A, B, C = "ABC"

    for i in range(2, n + 2):
        assert (C, i) not in data
        data[C, i] = Node([])

    for i in range(2, n + 2):
        for j in range(2, m + 2):
            assert (B, i, j) not in data
            if (B, i - 1, j) in data:
                data[B, i, j] = Node([data[B, i - 1, j]])
            else:
                data[B, i, j] = Node([])

    for i in range(2, n + 2):
        assert (A, i, 1, 1) not in data
        data[A, i, 1, 1] = Node([data[B, i, m + 1], data[C, n + 1]])

        for j in range(2, m + 2):
            for k in range(1, n + 1):
                assert (A, i, j, k) not in data
                data[A, i, j, k] = Node([data[A, i, j - 1, 1]])

    return data


def canonical_lpf(data):
    lpf = {}
    for node in data.values():
        lpf.setdefault(node.depth, []).append(node)
    order = sorted(lpf)
    assert tuple(order) == tuple(range(len(order)))
    return [lpf[k] for k in order]


def max_width(data):
    return max(len(row) for row in canonical_lpf(data))


def max_depth(data):
    return max(v.depth for v in data.values())


def main():
    from math import ceil, floor

    n_range = sorted(set(range(1, 10)) | set(range(1, 52, 10)))
    m_range = sorted(set(range(1, 10)) | set(range(1, 58, 14)))
    w_required = [False for _ in range(4)]
    for n in n_range:
        for m in m_range:
            data = make_data(n, m)

            # <@\ref{subsubsec:node-count} \hyperref[subsubsec:node-count]{Number of nodes}@>
            assert len(data) == 2 * n + m * n + m * n ** 2

            # <@\ref{subsubsec:worst-path} \hyperref[subsubsec:worst-path]{Critical path length}@>
            depth = max_depth(data)
            assert depth == n + m

            # <@\ref{subsubsec:clpf-width} \hyperref[subsubsec:clpf-width]{Canonical line parallel form width}@>
            w_a = n + m
            w_b = n * min(n - 2, m) + m + 1
            w_c = n * min(n - 1, m) + 1
            w_d = n * min(n, m)
            w_max = max(w_a, w_b, w_c, w_d)
            width = max_width(data)
            assert width == w_max

            # <@\noncopy{$w_a$ {\bf is used when} $n \in \left[1,\;2\right]$}@>
            if n in {1, 2}:
                assert w_a == w_max

            # <@\noncopy{$w_b$ {\bf is used when} $n \in \left[3,\;\ceil*{\frac{m}{2}}\right] \cup \left[m+2,\;+\infty\right]$}@>
            elif 3 <= n <= ceil(m / 2) or m + 2 <= n:
                assert w_b == w_max

            # <@\noncopy{$w_c$ {\bf is used when} $n = m+1$}@>
            elif n == m + 1:
                assert w_c == w_max

            # <@\noncopy{$w_d$ {\bf is used when} $n \in \left[\floor{\frac{m}{2}}+1,\;m\right]$}@>
            elif floor(m / 2) + 1 <= n <= m:
                assert w_d == w_max

            # A formula is required only if there are situations,
            # where all other formulas are suboptimal
            w_uses = [w == w_max for w in [w_a, w_b, w_c, w_d]]
            if sum(w_uses) == 1:
                w_used = w_uses.index(True)
                w_required[w_used] = True

            print(".", end="", flush=True)
        print()

    # Check that all <@\noncopy{$w$}@> formulas are required
    assert all(w_required)

    print("Assertions passed!")


if __name__ == "__main__":
    main()
