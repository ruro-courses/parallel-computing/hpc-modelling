def fmt_integer(contents, prefix="", postfix=""):
    return f"${prefix}{contents:d}{postfix}$"


def fmt_float(contents, prefix="", postfix=""):
    precision = max(0, 5 - len(f"{contents:.0f}"))
    precision = min(4, precision)
    return f"${prefix}{contents:.{precision}f}{postfix}$"


def fmt_scientific(contents, prefix="", postfix=""):
    mantissa, exponent = f"{contents:.1e}".split("e")
    return rf"${prefix}{mantissa}{{\cdot}}10^{{{exponent}}}{postfix}$"
