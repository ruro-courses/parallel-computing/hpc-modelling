import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

from .formatting import fmt_float, fmt_scientific
from .results import filter_results, get_results, process_results

mpl.rcParams["text.usetex"] = True


def compute_amdahl(speedup, comm_size):
    alpha = (1 / speedup - 1 / comm_size) / (1 - 1 / comm_size)
    alpha = np.round(alpha, 4)
    amdahl_comm_sizes = np.linspace(1, comm_size, 128)
    amdahl_speedups = 1 / (alpha + (1 - alpha) / amdahl_comm_sizes)
    return alpha, (amdahl_comm_sizes, amdahl_speedups)


def plot_speedup(results, host, mode, title="", seed=42):
    fig = plt.figure(figsize=(12.8, 4.6), tight_layout=True, dpi=200)

    (
        (epsilons, comm_sizes, num_trials),
        (times, speedups, _),
        (median_times, median_speedups, _),
    ) = process_results(filter_results(results, host, mode))

    for idx, epsilon in enumerate(epsilons):
        ax = fig.add_subplot(1, 3, 1 + idx)
        ax.set_xlim(min(comm_sizes) - 1, max(comm_sizes) + 1)
        ax.set_xticks(comm_sizes)
        ax.set_xlabel("Num. Processes")
        ax.set_ylabel("Relative Speedup")

        ax.set_title(
            fmt_scientific(epsilon, r"\varepsilon = ")
            + "\n"
            + fmt_float(median_times[idx, 0], r"T_1 = ", "s")
        )
        ax.plot(comm_sizes, comm_sizes, color="k", label="Best Theoretical Speedup")
        amdahl_alpha, amdahl_plot = compute_amdahl(
            median_speedups[idx, -1], comm_sizes[-1]
        )
        if amdahl_alpha > 0:
            ax.plot(
                *amdahl_plot,
                color="k",
                linestyle=":",
                label="Amdahl's law (" + fmt_float(amdahl_alpha, r"\alpha = ") + ")",
            )
        ax.scatter(
            np.tile(comm_sizes[:, None], num_trials),
            speedups[idx],
            s=2,
            color=f"C{idx}",
            label="Actual Speedup",
        )
        ax.plot(
            comm_sizes,
            median_speedups[idx],
            color=f"C{idx}",
            label="Actual Speedup (Median)",
        )
        ax.legend()

    fig.savefig(f"images/{host.lower()}_{mode.lower().replace('/', '_')}.png")


def main():
    data = get_results()
    delta = r"+\texttt{DELTA\_STOPPING}"
    adjusted = r"+\texttt{ADJUSTED\_SAMPLING}"
    plot_speedup(data, "bluegene", "ADJUSTED_SAMPLING/DELTA_STOPPING", delta + adjusted)
    plot_speedup(data, "bluegene", "FILTERED_SAMPLING/DELTA_STOPPING", delta)
    plot_speedup(data, "bluegene", "FILTERED_SAMPLING/ERROR_STOPPING")
    plot_speedup(data, "polus", "ADJUSTED_SAMPLING/DELTA_STOPPING", delta + adjusted)
    plot_speedup(data, "polus", "FILTERED_SAMPLING/DELTA_STOPPING", delta)
    plot_speedup(data, "polus", "FILTERED_SAMPLING/ERROR_STOPPING")


if __name__ == "__main__":
    main()
