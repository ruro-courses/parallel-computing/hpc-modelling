import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import gaussian_kde

from .samples import CONE_AREA, PARALLELEPIPED_AREA, generate_points

mpl.rcParams["text.usetex"] = True


def main():
    np.random.seed(42)
    fig = plt.figure(figsize=(12.8, 6.3), dpi=200)
    veps = 0.02
    heps = 0.0
    fig.subplots_adjust(
        top=1 - heps,
        right=1 - veps,
        bottom=heps,
        left=veps,
        hspace=3 * heps,
        wspace=3 * veps,
    )
    axes = []
    for idx, strategy in enumerate(["filtered", "naive", "adjusted"]):
        ax = fig.add_subplot(1, 3, 1 + idx, projection="3d")
        ax.set_xticks(np.linspace(-1, 1, 5))
        ax.set_yticks(np.linspace(-1, 1, 5))
        ax.set_zticks(np.linspace(0, 1, 5))
        ax.set_xlim(-1, 1)
        ax.set_ylim(-1, 1)
        ax.set_zlim(0, 1)
        ax.set_xlabel("x")
        ax.set_ylabel("y")
        ax.set_zlabel("z")

        x, y, z = generate_points(strategy, n=10_000)
        xyz = np.vstack([x, y, z])
        c = gaussian_kde(xyz)(xyz)
        c = c / c.max()

        ax.set_title(f"{strategy.capitalize()}\n({x.size} points)")
        sc = ax.scatter(
            x,
            y,
            z,
            c=c,
            s=1,
            vmin=0,
            vmax=1,
        )
        axes.append(ax)

    cbar = fig.colorbar(
        sc,
        orientation="horizontal",
        ax=axes,
    )
    cbar.ax.set_title(label="Relative Point Density")

    fig.savefig("images/samples.png")


if __name__ == "__main__":
    main()
