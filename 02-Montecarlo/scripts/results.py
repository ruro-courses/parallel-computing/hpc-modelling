import pathlib

import numpy as np
import parse


def read_result(path):
    host = path.parent.name.split("_", 1)[-1]
    with path.open() as f:
        lines = [line for line in f.readlines() if "__TRIALS__" in line]
        if "]" not in lines[-1]:
            print(f"Incomplete logs for run {path} (this is probably okay).")
            lines.append("]\n")

    mode, epsilon, comm_size, seed = parse.parse(
        "# Running __TRIALS__ with {} and "
        "epsilon={:.e}, comm_size={:d}, seed={:d}\n",
        lines[0],
    )

    module = {}
    exec("\n".join(lines), module)
    trials = np.array(module["__TRIALS__"])
    results = {
        "comm_size": comm_size,
        "epsilon": epsilon,
        "host": host,
        "mode": mode,
        "seed": seed,
        "trials": trials,
    }
    assert trials.size, path
    return results


def get_results():
    results = {}
    for file in pathlib.Path().glob("results_*/*.out"):
        result = read_result(file)
        value = result.pop("trials")
        key = tuple(v for k, v in sorted(result.items()))
        assert key not in results, key
        results[key] = value
    return results


def filter_results(results, host, mode):
    results = {k[:2]: v for k, v in results.items() if k[2] == host and k[3] == mode}
    return results


def process_results(results):
    epsilons = np.array(sorted(set(k[1] for k in results), reverse=True))
    comm_sizes = np.array(sorted(set(k[0] for k in results)))
    num_trials = max(v.shape[0] for v in results.values())

    times = np.full((len(epsilons), len(comm_sizes), num_trials), np.nan)
    median_times = np.empty((len(epsilons), len(comm_sizes)))

    errors = np.full((len(epsilons), len(comm_sizes), num_trials), np.nan)
    median_errors = np.empty((len(epsilons), len(comm_sizes)))

    for idx, epsilon in enumerate(epsilons):
        for jdx, comm_size in enumerate(comm_sizes):
            time_data = results[comm_size, epsilon][:, 0]
            times[idx, jdx, : time_data.shape[-1]] = time_data
            median_times[idx, jdx] = np.median(time_data)

            error_data = results[comm_size, epsilon][:, 3]
            errors[idx, jdx, : error_data.shape[-1]] = error_data
            median_errors[idx, jdx] = np.median(error_data)

    median_speedups = median_times[..., :1] / median_times
    speedups = median_times[..., :1, None] / times

    return (
        (epsilons, comm_sizes, num_trials),
        (times, speedups, errors),
        (median_times, median_speedups, median_errors),
    )
