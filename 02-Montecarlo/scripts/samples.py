import numpy as np

PARALLELEPIPED_AREA = 4
CONE_AREA = np.pi / 3


def generate_points(strategy, n):
    assert strategy in {"naive", "filtered", "adjusted"}
    if strategy == "filtered":
        return generate_points_filtered(n)
    else:
        return generate_points_conical(n, adjusted=strategy == "adjusted")


def generate_points_filtered(n):
    a = np.random.uniform(size=n)
    b = np.random.uniform(size=n)
    c = np.random.uniform(size=n)

    x = 2 * a - 1
    y = 2 * b - 1
    z = c

    valid = x * x + y * y < z * z
    return x[valid], y[valid], z[valid]


def generate_points_conical(n, adjusted=False):
    a = np.random.uniform(size=n)
    b = np.random.uniform(size=n)
    c = np.random.uniform(size=n)

    if adjusted:
        z = np.cbrt(a)
        r = z * np.sqrt(b)
    else:
        z = a
        r = z * b
    t = 2 * np.pi * c

    x = r * np.cos(t)
    y = r * np.sin(t)
    return x, y, z
