import subprocess

from .formatting import fmt_float, fmt_integer, fmt_scientific
from .results import filter_results, get_results, process_results


def make_centered_cell(contents):
    return r"\multicolumn{1}{|c|}{" + contents + "}"


def make_merged_cell(contents, height):
    return r"\multirow{" + str(height) + r"}{*}{" + contents + "}"


def make_line(*contents):
    return r"&".join(contents) + r"\\" + "\n"


def make_header():
    return make_line(
        make_centered_cell(r"$\varepsilon$"),
        make_centered_cell(r"$p$"),
        make_centered_cell(r"$T$"),
        make_centered_cell(r"$S$"),
        make_centered_cell(r"$\Delta V$"),
    )


def make_block(epsilon, comm_sizes, *medians):
    epsilon_cell = make_merged_cell(fmt_scientific(epsilon), len(comm_sizes))

    block_lines = []
    for comm_size, time, speedup, error in zip(comm_sizes, *medians):
        comm_size_cell = fmt_integer(comm_size)
        time_cell = fmt_float(time, postfix="s")
        speedup_cell = fmt_float(speedup, postfix=r"\times")
        error_cell = fmt_scientific(error)
        block_lines.append(
            make_line(
                epsilon_cell,
                comm_size_cell,
                time_cell,
                speedup_cell,
                error_cell,
            )
        )
        epsilon_cell = ""

    return r"\cline{2-5}".join(block_lines)


def make_tabular(epsilons, comm_sizes, *medians):
    tabular_blocks = [make_header()]
    for epsilon, *median in zip(epsilons, *medians):
        tabular_blocks.append(make_block(epsilon, comm_sizes, *median))

    return (
        r"\begin{tabular}{|r|r|r|r|r|}"
        "\n"
        r"\hline" + r"\hline\hline".join(tabular_blocks) + r"\hline"
        "\n"
        r"\end{tabular}"
    )


def make_table(results, host, mode, title=""):
    (
        (epsilons, comm_sizes, _),
        _,
        medians,
    ) = process_results(filter_results(results, host, mode))

    table = (
        r"\caption{Результаты\\("
        f"{host.capitalize()}{title}"
        ")}\n"
        r"\scriptsize"
        "\n" + make_tabular(epsilons, comm_sizes, *medians)
    )

    path = f"results_{host.lower()}/{mode.lower().replace('/', '_')}.tex"
    with open(path, "w") as f:
        f.write(table)

    subprocess.check_output(["latexindent", f"--outputfile={path}", path])


def main():
    data = get_results()
    delta = r"+\texttt{DELTA\_STOPPING}"
    adjusted = r"+\texttt{ADJUSTED\_SAMPLING}"
    make_table(data, "bluegene", "ADJUSTED_SAMPLING/DELTA_STOPPING", delta + adjusted)
    make_table(data, "bluegene", "FILTERED_SAMPLING/DELTA_STOPPING", delta)
    make_table(data, "bluegene", "FILTERED_SAMPLING/ERROR_STOPPING")
    make_table(data, "polus", "ADJUSTED_SAMPLING/DELTA_STOPPING", delta + adjusted)
    make_table(data, "polus", "FILTERED_SAMPLING/DELTA_STOPPING", delta)
    make_table(data, "polus", "FILTERED_SAMPLING/ERROR_STOPPING")


if __name__ == "__main__":
    main()
