import time

import numpy as np

from .samples import CONE_AREA, PARALLELEPIPED_AREA, generate_points


def integrate(strategy, n):
    if strategy == "filtered":
        area = PARALLELEPIPED_AREA
    else:
        area = CONE_AREA

    # You can generate less points to get the same precision if you don't need
    # to filter the points, since all the points you generate will be used.
    n = np.ceil(n * area / PARALLELEPIPED_AREA).astype(np.int_).item()

    x, y, z = generate_points(strategy, n)
    values = np.sqrt(x * x + y * y)
    return area * values.sum() / n


def main():
    np.random.seed(42)
    n = 10_000_000
    value = np.pi / 6
    print(f"{'Reference':>10}: {value = :0.9f}")
    for strategy in ["filtered", "adjusted"]:
        start = time.time()
        value = integrate(strategy, n)
        error = np.abs(value - np.pi / 6)
        end = time.time()
        duration = end - start
        strategy = strategy.capitalize()
        print(
            f"{strategy:>10}: {value = :0.9f} | "
            f"{error = :0.4e} | {duration = :0.4f}s"
        )


if __name__ == "__main__":
    main()
