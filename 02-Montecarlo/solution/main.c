#include "forwards.h"
#include "boilerplate.c"
#include "trial.c"
#include "xoshiro256plus.c"

int main(int argc, char *argv[])
{
    // region Parse arguments
    init(&argc, &argv);
    if (PERIOD % comm_size)
        stop(1, "Sampling period (%d) must be divisible by the world size (%d).\n", PERIOD, comm_size);

    if (argc != 3)
        stop(1, "Usage: %s [epsilon] [seed]\n", argv[0]);

    double epsilon;
    if (sscanf(argv[1], "%lf", &epsilon) != 1)
        stop(1, "Couldn't parse [epsilon] as a double.");

    uint64_t seed;
    if (sscanf(argv[2], "%" SCNu64, &seed) != 1)
        stop(1, "Couldn't parse [seed] as an unsigned 64 bit integer.");
    // endregion

    // region Initialize PRNG
    trial_t trial;
    seed_state(trial.state, seed);
    for (int i = 0; i < comm_rank; ++i)
        long_jump(trial.state);
    // endregion

    // region Run trials
    root_printf(
        "# Running __TRIALS__ with " SAMPLING_MODE "/" STOPPING_MODE " and "
        "epsilon=%le, comm_size=%d, seed=%" PRIu64 "\n",
        epsilon, comm_size, seed
    );
    root_printf("__TRIALS__ = [\n");
    root_printf("#   (trial_time, trial_iters, trial_points, trial_error),\n");
    for (uint64_t idx = 0; idx < TRIALS; ++idx)
    {
        do_trial(epsilon, &trial);
        root_printf(
            "    (%le, %" PRIu64 ", %" PRIu64 ", %le),  # __TRIALS__\n",
            trial.time, trial.iters, trial.points, trial.error
        );
    }
    root_printf("]  # __TRIALS__\n");
    stop(0, "# Done.\n");
    // endregion
}

