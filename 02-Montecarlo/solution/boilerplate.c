#include "forwards.h"

int comm_size, comm_rank;

void init(int *argc, char **argv[])
{
    MPI_Init(argc, argv);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &comm_rank);
}

#define root_printf(...) do                     \
    {                                           \
        if (!comm_rank)                         \
            printf(__VA_ARGS__);                \
    }                                           \
    while(false)

#define stop(code, ...) do                      \
    {                                           \
        root_printf(__VA_ARGS__);               \
        MPI_Finalize();                         \
        exit(code);                             \
    }                                           \
    while(false)
