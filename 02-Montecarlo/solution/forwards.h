#pragma once

#include <float.h>
#include <inttypes.h>
#include <limits.h>
#include <math.h>
#include <mpi.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef M_PI
#define M_PI (3.14159265358979323846264338327950288)
#endif

#ifndef MPI_UINT64_T
#define MPI_UINT64_T MPI_UNSIGNED_LONG_LONG

#if ULLONG_MAX != UINT64_MAX
#error MPI_UINT64_T is not defined and unsigned long long is not a 64 bit type.
#endif
#endif

#ifdef ADJUSTED_SAMPLING
// Sample only points inside the cone
#define SAMPLING_MODE "ADJUSTED_SAMPLING"
// Right circular cone (h=1, r=1)
#define REGION_AREA (M_PI / 3)
#else
// Sample all points and filter them
#define SAMPLING_MODE "FILTERED_SAMPLING"
// Parallelepiped (w=2, h=2, d=1)
#define REGION_AREA (4)
#endif

#ifdef DELTA_STOPPING
// Stop when the upper bound on the delta is less than epsilon
#define STOPPING_MODE "DELTA_STOPPING"
#else
// Stop when the error is less than epsilon
#define STOPPING_MODE "ERROR_STOPPING"
#endif

// Use highly composite number for PERIOD so that
// it cleanly divides most COMM_SIZE values.
#define PERIOD 6720
#define TRIALS 10
#define EXACT_VALUE (M_PI / 6)

typedef uint64_t prng_state_t[4];

typedef struct
{
    double time;
    double error;
    double accum;
    uint64_t iters;
    uint64_t points;
    prng_state_t state;
} trial_t;

void jump(prng_state_t s);
void long_jump(prng_state_t s);
void seed_state(prng_state_t s, uint64_t seed);
double next_double(prng_state_t s);
