#include "forwards.h"

double get_value(trial_t *trial)
{ return REGION_AREA * trial->accum / trial->iters; }

void do_trial_period(trial_t *trial)
{
    double local_accum = 0;
    uint64_t local_points = 0;
    for (uint64_t iter = 0; iter < PERIOD; iter += comm_size)
    {
        double a = next_double(trial->state);
        double b = next_double(trial->state);
        double c = next_double(trial->state);
#ifdef ADJUSTED_SAMPLING
        double z = cbrt(a);
        double r = z * sqrt(b);
        double t = 2 * M_PI * c;

        double x = r * cos(t);
        double y = r * sin(t);
#else
        double x = 2 * a - 1;
        double y = 2 * b - 1;
        double z = c;

        if (x * x + y * y > z * z) continue;
#endif

        (void) z;
        local_accum += sqrt(x * x + y * y);
        local_points += 1;
    }
    double global_accum;
    uint64_t global_points;
    MPI_Allreduce(&local_accum, &global_accum, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    MPI_Allreduce(&local_points, &global_points, 1, MPI_UINT64_T, MPI_SUM, MPI_COMM_WORLD);

    trial->accum += global_accum;
    trial->points += global_points;
    trial->iters += PERIOD;
    trial->error = fabs(get_value(trial) - EXACT_VALUE);
}

void do_trial(double epsilon, trial_t *trial)
{
    MPI_Barrier(MPI_COMM_WORLD);
    double tstart = MPI_Wtime();

    trial->error = DBL_MAX;
    trial->accum = 0;
    trial->iters = 0;
    trial->points = 0;
    while (
#ifdef DELTA_STOPPING
        (REGION_AREA * PERIOD / (double) trial->iters > epsilon)
#else
        (trial->error > epsilon)
#endif
    )
        do_trial_period(trial);

    MPI_Barrier(MPI_COMM_WORLD);
    double tend = MPI_Wtime();

    trial->time = tend - tstart;
}
