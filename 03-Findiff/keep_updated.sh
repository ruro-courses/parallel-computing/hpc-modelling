#!/bin/sh
FILE=${1:-report}
evince "${FILE}.pdf" >/dev/null 2>/dev/null &
find -type f | rg -v 'output' | rg '(tex|bib|png|jpg)' | sort | \
    entr -as "./update_pdf.sh ${FILE}.tex && evince ${FILE}.pdf"
