#include "10_includes.h"
#include "20_macros/20_macros.h"
#include "30_typedefs/30_typedefs.h"
#include "40_init/40_init.c"
#include "50_impl/50_impl.c"

int main(int argc, char *argv[])
{
    init_underscore(argc, argv);

    // Dump the current global state
    critical_section(dump_underscore, __);

    for (int trial = 0; trial < 10; ++trial)
        run();

    fini_underscore();
}
