void init_local(void)
{
    MPI_Dims_create(__.world.size, 3, __.local.shape.m);
    MPI_Cart_create(__.world.comm, 3, __.local.shape.m, __.param.period.m, true, &__.local.comm);
    MPI_Comm_rank(__.local.comm, &__.local.rank);
    MPI_Cart_coords(__.local.comm, __.local.rank, 3, __.local.coord.m);
}

void fini_local(void)
{
    MPI_Comm_free(&__.local.comm);
}
