void init_block(void)
{
    // block.shape = ceil(nodes.shape / local.shape)
#define ceil_div(n, d) (((n) + (d) - 1) / (d))
#define init_block_shape(sym)                                                   \
    ceil_div(__.nodes.shape.sym, __.local.shape.sym)
    broadcast_vec3(set_field_fun, __.block.shape, init_block_shape);
#undef init_block_shape
#undef ceil_div

    // block.coord = local.coord * block.shape
#define init_block_coord(sym)                                                   \
    __.local.coord.sym * __.block.shape.sym
    broadcast_vec3(set_field_fun, __.block.coord, init_block_coord);
#undef init_block_coord

    // Clip block.shape when it is the last block
    // Then add extra 2 elements (for edges)
#define min(a, b) ((a) < (b) ? (a) : (b))
#define clip_block_shape(sym)                                                   \
    2 + min(__.block.shape.sym, __.nodes.shape.sym - __.block.coord.sym)
    broadcast_vec3(set_field_fun, __.block.shape, clip_block_shape);
#undef clip_block_shape
#undef min

    // Allocate the initial buffer arrays
    __.block.elems = _broadcast_vec3((*), get_field, __.block.shape);
    broadcast_vec3(set_field_fun, __.block.array, alloc_block_array);

    // Initialize MPI subarray types that allow us to "slice"
    // an edge directly from the buffer without copying it first
#define init_block_types(sym) do                                                \
    {                                                                           \
        vec3_int_t zeros3 = {0};                                                \
        vec3_int_t slice_shape = __.block.shape;                                \
        slice_shape.sym = 1;                                                    \
        MPI_Type_create_subarray(                                               \
            3,                                                                  \
            __.block.shape.m,                                                   \
            slice_shape.m,                                                      \
            zeros3.m,                                                           \
            MPI_ORDER_C,                                                        \
            MPI_DOUBLE,                                                         \
            &__.block.types.sym                                                 \
        );                                                                      \
        MPI_Type_commit(&__.block.types.sym);                                   \
    }                                                                           \
    while(false)
    broadcast_vec3(init_block_types);
#undef init_block_types
}

void fini_block()
{
    broadcast_vec3(call_field_fun, __.block.array, free);
    broadcast_vec3(call_field_fun, &__.block.types, MPI_Type_free);
}

