void init_world(void)
{
    __.world.comm = MPI_COMM_WORLD;
    MPI_Comm_size(__.world.comm, &__.world.size);
    MPI_Comm_rank(__.world.comm, &__.world.rank);
}
