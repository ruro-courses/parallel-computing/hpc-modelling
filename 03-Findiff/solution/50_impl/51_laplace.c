#define laplace_1d(sym, src, dst) do                                            \
    {                                                                           \
        const double inv_h2 = 1 / (                                             \
            _.nodes.steps.sym * _.nodes.steps.sym                               \
        );                                                                      \
                                                                                \
        /* for each inner element */                                            \
        OMP_PRAGMA("omp for schedule(static)")                                  \
        foreach(x, elm, _.block.shape)                                          \
            foreach(y, elm, _.block.shape)                                      \
                foreach(z, elm, _.block.shape)                                  \
                {                                                               \
                    /* get previous, current and next elements */               \
                    vec3_int_t cur = get_index(elm);                            \
                    vec3_int_t prv = cur; prv.sym -= 1;                         \
                    vec3_int_t nxt = cur; nxt.sym += 1;                         \
                    double u_cur = src[s_(cur)];                                \
                    double u_prv = src[s_(prv)];                                \
                    double u_nxt = src[s_(nxt)];                                \
                                                                                \
                    /* compute the partial finite-difference */                 \
                    /* approximation for the laplace operator */                \
                    double u_dif = u_nxt - 2.0 * u_cur + u_prv;                 \
                    dst[s_(cur)] += inv_h2 * u_dif;                             \
                }                                                               \
    }                                                                           \
    while(false)

void laplace_3d(const double * restrict src, double * restrict dst)
{
    READONLY_UNDERSCORE(_);

    OMP_PRAGMA("omp parallel default(none) shared(_, src, dst)")
    {
        // Accumulate the partial laplace along all 3 dimensions
        broadcast_vec3(laplace_1d, src, dst);
    }
}
#undef laplace_1d
