void step0(void)
{
    READONLY_UNDERSCORE(_);

    // Initialize the first layer with the analytical solution
    set_analytical(0, _.block.array.m[2]);
}

void step1(void)
{
    READONLY_UNDERSCORE(_);

    // Compute the laplace operator
    laplace_3d(_.block.array.m[1], _.block.array.m[2]);
    double t2_2 = (_.nodes.steps.t * _.nodes.steps.t) / 2.0;

    // for each inner element
    foreach(x, elm, _.block.shape)
        foreach(y, elm, _.block.shape)
            foreach(z, elm, _.block.shape)
            {
                vec3_int_t cur = get_index(elm);

                // Get the value from previous steps and the current laplace value
                double u0 = _.block.array.m[1][s_(cur)];
                double lp = _.block.array.m[2][s_(cur)];

                // Compute the next value
                _.block.array.m[2][s_(cur)] = t2_2 * lp + u0;
            }
}

void stepN(void)
{
    READONLY_UNDERSCORE(_);

    // Compute the laplace operator
    laplace_3d(_.block.array.m[1], _.block.array.m[2]);
    double t2 = _.nodes.steps.t * _.nodes.steps.t;

    // for each inner element
    foreach(x, elm, _.block.shape)
        foreach(y, elm, _.block.shape)
            foreach(z, elm, _.block.shape)
            {
                vec3_int_t cur = get_index(elm);

                // Get the values from 2 previous steps and the current laplace value
                double u0 = _.block.array.m[0][s_(cur)];
                double u1 = _.block.array.m[1][s_(cur)];
                double lp = _.block.array.m[2][s_(cur)];

                // Compute the next value
                _.block.array.m[2][s_(cur)] = t2 * lp + 2.0 * u1 - u0;
            }
}

double step(int num)
{
    // Rotate buffers
    free(__.block.array.m[0]);
    __.block.array.m[0] = __.block.array.m[1];
    __.block.array.m[1] = __.block.array.m[2];
    __.block.array.m[2] = alloc_block_array();

    // Do the timestep
    if (num == 0) step0();
    else if (num == 1) step1();
    else stepN();

    // Exchange and update the edge values
    edges_3d(__.block.array.m[2]);

    // Compute the maximum error
    return max_analytical_error(num, __.block.array.m[2]);
}
