#define exchange(sym, other1, other2, arr) do                                   \
    {                                                                           \
        /* find the previous and next blocks */                                 \
        int prv_idx, nxt_idx;                                                   \
        MPI_Cart_shift(                                                         \
            _.local.comm,                                                       \
            (*#sym) - 'x', 1,                                                   \
            &prv_idx, &nxt_idx                                                  \
        );                                                                      \
                                                                                \
        vec3_int_t src_elm = {0}, dst_elm = {0};                                \
        src_elm.other1 = 0;                                                     \
        src_elm.other2 = 0;                                                     \
        dst_elm.other1 = 0;                                                     \
        dst_elm.other2 = 0;                                                     \
                                                                                \
        /* prv[-1] = cur[1]; cur[-1] = nxt[1]; */                               \
        src_elm.sym = 1;                                                        \
        dst_elm.sym = _.block.shape.sym - 1;                                    \
                                                                                \
        MPI_Sendrecv(                                                           \
            /* send data to previous block */                                   \
            &arr[s_(src_elm)], 1, _.block.types.sym,                            \
            prv_idx, 0,                                                         \
            /* recv data from next block */                                     \
            &arr[s_(dst_elm)], 1, _.block.types.sym,                            \
            nxt_idx, MPI_ANY_TAG,                                               \
            _.local.comm, MPI_STATUSES_IGNORE                                   \
        );                                                                      \
                                                                                \
        /* nxt[0] = cur[-2]; cur[0] = prv[-2]; */                               \
        src_elm.sym = _.block.shape.sym - 2;                                    \
        dst_elm.sym = 0;                                                        \
                                                                                \
        MPI_Sendrecv(                                                           \
            /* send data to next block */                                       \
            &arr[s_(src_elm)], 1, _.block.types.sym,                            \
            nxt_idx, 0,                                                         \
            /* recv data from previous block */                                 \
            &arr[s_(dst_elm)], 1, _.block.types.sym,                            \
            prv_idx, MPI_ANY_TAG,                                               \
            _.local.comm, MPI_STATUSES_IGNORE                                   \
        );                                                                      \
    }                                                                           \
    while(false)

#define condition(sym, other1, other2, arr)                                     \
    /* if not a periodic edge and not in inner block */                         \
    if (!_.param.period.sym && !_.local.coord.sym)                              \
    {                                                                           \
        int elm ## sym = 1;                                                     \
        OMP_PRAGMA("omp for schedule(static)")                                  \
        foreach(other1, elm, _.block.shape)                                     \
            foreach(other2, elm, _.block.shape)                                 \
            {                                                                   \
                /* fill the edge with zeroes */                                 \
                arr[s_(get_index(elm))] = 0;                                    \
            }                                                                   \
    }

void edges_3d(double *arr)
{
    READONLY_UNDERSCORE(_);

    // Swap data with neighbour processes
    broadcast_vec3_full(exchange, arr);

    OMP_PRAGMA("omp parallel default(none) shared(_, arr)")
    {
        // Enforce first-type boundary condition
        broadcast_vec3_full(condition, arr);
    }
}

#undef exchange
#undef condition
