// np.sin(A * p + B).prod(axis=-1)
// Also, instead of cos(a_t * t + b_t), we compute sin(... + pi/2)
#define formula(sym, pos)                                                       \
    sin(_.param.a.sym * pos.sym + _.param.b.sym)

#define with_analytical(callback, step, ...) do                                 \
    {                                                                           \
        /* for each inner element */                                            \
        OMP_PRAGMA("omp for schedule(static)")                                  \
        foreach(x, elm, _.block.shape)                                          \
            foreach(y, elm, _.block.shape)                                      \
                foreach(z, elm, _.block.shape)                                  \
                {                                                               \
                    /* get the element index and floating coordinates */        \
                    vec3_int_t cur = get_index(elm);                            \
                    vec4_f64_t pos = get_coord(cur, step);                      \
                                                                                \
                    /* compute the analytical values of the function */         \
                    double val = _broadcast_vec4((*), formula, pos);            \
                                                                                \
                    /* run the callback with the computed value */              \
                    callback(cur, val, ##__VA_ARGS__);                          \
                }                                                               \
    }                                                                           \
    while(false)

void set_analytical(int step, double * restrict dst)
{
    READONLY_UNDERSCORE(_);

    OMP_PRAGMA("omp parallel default(none) shared(_, dst, step)")
    {
        // Compute the analytical solution and store it into dst
#define set_value(cur, val, dst)                                                \
    dst[s_(cur)] = val
        with_analytical(set_value, step, dst);
#undef set_value
    }
}

double max_analytical_error(int step, const double * ref)
{
    READONLY_UNDERSCORE(_);
    double err = 0.0;

    OMP_PRAGMA("omp parallel default(none) shared(_, ref, step, err)")
    {
        double err_ = 0.0;

        // Compute analytical solution. Use it to find the maximum error in ref.
        // The analytical solution isn't stored, no extra memory is used.
#define update_error(cur, val, ref, err)                                        \
    err = fmax(err, fabs(val - ref[s_(cur)]))
        with_analytical(update_error, step, ref, err_);
#undef update_error

        // OpenMP 2.5 doesn't have reduction(max)
        // so we do the reduction manually
        OMP_PRAGMA("omp critical") err = fmax(err, err_);
    }
    return err;
}

#undef formula
#undef with_analytical
