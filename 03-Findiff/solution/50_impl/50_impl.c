#include "51_laplace.c"
#include "52_edges.c"
#include "53_analytical.c"
#include "54_step.c"

void run(void)
{
    READONLY_UNDERSCORE(_);

    // Start the timer
    MPI_Barrier(_.local.comm);
    double tstart = MPI_Wtime();

    // Run for K timesteps
    double errors[_.nodes.shape.t + 1];
    for (int t = 0; t <= _.nodes.shape.t; ++t)
    {
        double local_error = step(t);

        // Reduce the error over all processes
        MPI_Allreduce(&local_error, &errors[t], 1, MPI_DOUBLE, MPI_MAX, _.local.comm);
    }

    // Stop the timer
    MPI_Barrier(_.local.comm);
    double time = MPI_Wtime() - tstart;

    // Report results
    root_printf(_.local, "__TRIALS__.append((%le, [", time);
    for (int t = 0; t <= _.nodes.shape.t; ++t)
        root_printf(_.local, "%le, ", errors[t]);
    root_printf(_.local, "]))\n");
}
