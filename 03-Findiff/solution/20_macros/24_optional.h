// Remove OpenMP macros when USE_OPENMP is not defined
#ifdef USE_OPENMP
    #include <omp.h>
    #define OMP_PRAGMA(x) _Pragma(x)
#else
    #define omp_get_max_threads() 0
    #define OMP_PRAGMA(x)
#endif
