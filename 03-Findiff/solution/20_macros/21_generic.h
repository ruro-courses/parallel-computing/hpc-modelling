// Math
#ifndef M_PI
    #define M_PI (3.14159265358979323846264338327950288)
#endif

// Helper macros
#define unparen(...) __VA_ARGS__
#define str(v) #v
#define READONLY_UNDERSCORE(u) const underscore_t u = __

// Executes action, one process at a time
#define critical_section(action, ...) do                                        \
    {                                                                           \
        for (int _i = 0; _i < __.world.size; ++_i)                              \
        {                                                                       \
            MPI_Barrier(__.world.comm);                                         \
            if (_i == __.world.rank)                                            \
            {                                                                   \
                action(__VA_ARGS__);                                            \
                fflush(stdout);                                                 \
            }                                                                   \
        }                                                                       \
        MPI_Barrier(__.world.comm);                                             \
    }                                                                           \
    while(false)

// Prints message in root process only
#define root_printf(world, ...) do                                              \
    {                                                                           \
        if (!world.rank)                                                        \
            printf(__VA_ARGS__);                                                \
    }                                                                           \
    while(false)

// Prints an error and terminates the program early
#define stop(code, ...) do                                                      \
    {                                                                           \
        root_printf(__.world, __VA_ARGS__);                                     \
        MPI_Finalize();                                                         \
        exit(code);                                                             \
    }                                                                           \
    while(false)
