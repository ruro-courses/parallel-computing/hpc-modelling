#!/bin/sh
HOSTS=5
PROCS="${1}"
PTILE="$(( (PROCS+HOSTS-1) / HOSTS ))"

shift

bsub <<EOF
source /polusfs/setenv/setup.SMPI
#BSUB -n ${PROCS}
#BSUB -W 00:15
#BSUB -x
#BSUB -R "span[ptile=${PTILE}]"
#BSUB -o main.%J.out
#BSUB -e main.%J.err
OMP_NUM_THREADS=8 mpiexec ${@}
EOF
