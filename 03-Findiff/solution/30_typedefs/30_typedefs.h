// Declare vec3/vec4 types
make_vecN_t(3, buf, double *);
make_vecN_t(3, int, int);
make_vecN_t(3, typ, MPI_Datatype);
make_vecN_t(4, f64, double);
make_vecN_t(4, int, int);

#include "31_world.h"
#include "32_nodes.h"
#include "33_param.h"
#include "34_local.h"
#include "35_block.h"

// The underscore object contains the global state of the program
// You can use it like this:
//     __.world.blah.blah
// Or like this:
//     READONLY_UNDERSCORE(_);
//     _.world.blah.blah
// The first variant has read-write access, but is slightly slower than the
// second one. The second one is slightly faster to access, but it's read-only
// and doesn't reflect any changes done to __ after the READONLY_UNDERSCORE call.
typedef struct
{
    // Global
    world_t world;
    nodes_t nodes;
    param_t param;
    // Local
    local_t local;
    block_t block;
} underscore_t;

// Debug print most of the underscore state information
#define dump_underscore(und) do                                                 \
    {                                                                           \
        printf("=======================\n");                                    \
        dump_world(und.world); printf("\n");                                    \
        dump_nodes(und.nodes); printf("\n");                                    \
        dump_param(und.param); printf("\n");                                    \
        dump_local(und.local); printf("\n");                                    \
        dump_block(und.block); printf("\n");                                    \
        fflush(stdout);                                                         \
    }                                                                           \
    while(false)
