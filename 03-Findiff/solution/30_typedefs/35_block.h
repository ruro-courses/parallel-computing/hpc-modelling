// Process-local block
typedef struct
{
    vec3_int_t shape; // nx ny nz
    vec3_int_t coord; // ix iy iz
    vec3_typ_t types; // array slice types
    int elems; // buffer array size
    vec3_buf_t array; // u0 u1 u2
} block_t;

// Debug print most of the block state information
#define dump_block(block) do                                                    \
    {                                                                           \
        dump_vecN(3, block.shape, "d");                                         \
        dump_vecN(3, block.coord, "d");                                         \
        dump_field(block.elems, "d");                                           \
    }                                                                           \
    while(false)

// Allocate 0-initialized memory to be used as a buffer array
#define alloc_block_array(...)                                                  \
    calloc(__.block.elems, sizeof(double))
