// Values describing the analytical solution, initial and edge conditions
typedef struct
{
    vec3_int_t period; // Periodic or first-type boundary condition
    vec4_f64_t a; // Multiplicative coefficients
    vec4_f64_t b; // Additive coefficients
} param_t;

// Debug print most of the param state information
#define dump_param(param) do                                                    \
    {                                                                           \
        dump_vecN(3, param.period, "d");                                        \
        dump_vecN(4, param.a, "lf");                                            \
        dump_vecN(4, param.b, "lf");                                            \
    }                                                                           \
    while(false)
