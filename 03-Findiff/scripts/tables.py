import subprocess

import numpy as np

from .results import Results, get_baselines, same_reduce


def fmt_integer(contents, prefix="", postfix=""):
    return f"${prefix}{contents:d}{postfix}$"


def fmt_float(contents, prefix="", postfix=""):
    precision = max(0, 5 - len(f"{contents:.0f}"))
    precision = min(4, precision)
    return f"${prefix}{contents:.{precision}f}{postfix}$"


def fmt_scientific(contents, prefix="", postfix=""):
    mantissa, exponent = f"{contents:.1e}".split("e")
    return rf"${prefix}{mantissa}{{\cdot}}10^{{{exponent}}}{postfix}$"


def make_centered_cell(contents):
    return r"\multicolumn{1}{|c|}{" + contents + "}"


def make_merged_cell(contents, height):
    return r"\multirow{" + str(height) + r"}{*}{" + contents + "}"


def make_line(*contents):
    return r"&".join(contents) + r"\\" + "\n"


def make_header(bluegene=False):
    return make_line(
        make_centered_cell(r"$N$"),
        make_centered_cell(r"$P$"),
        make_centered_cell(r"$T$"),
        make_centered_cell(r"$S$"),
        *(
            [
                make_centered_cell(r"$T_{omp}$"),
                make_centered_cell(r"$S_{omp}$"),
                make_centered_cell(r"$T/T_{omp}$"),
            ]
            if bluegene
            else []
        ),
        make_centered_cell(r"$\delta (L=1)$"),
        make_centered_cell(r"$\delta (L=\pi)$"),
    )


def missing():
    return 5 * [make_centered_cell(r"\textbf{---}")]


def make_block(N, Ps, *stats, bluegene=False):
    *stats, errors = stats
    N_cell = make_merged_cell(fmt_integer(N, postfix="^3"), len(Ps))
    error_cells = [make_merged_cell(fmt_scientific(e), len(Ps)) for e in errors]

    block_lines = []
    for P, time, speedup, *extras in zip(Ps, *stats):
        P_cell = fmt_integer(P)
        if np.isnan(speedup):
            block_lines.append(
                make_line(
                    N_cell,
                    P_cell,
                    *missing(),
                    *error_cells,
                )
            )
            N_cell = ""
            error_cells = ["" for c in error_cells]
            continue
        time_cell = fmt_float(time, postfix="s")
        speedup_cell = fmt_float(speedup, postfix=r"\times")
        if bluegene:
            time_omp, speedup_omp = extras
            extra_cells = [
                fmt_float(time_omp, postfix="s"),
                fmt_float(speedup_omp, postfix=r"\times"),
                fmt_float(time / time_omp, postfix=r"\times"),
            ]
        else:
            extra_cells = []

        block_lines.append(
            make_line(
                N_cell,
                P_cell,
                time_cell,
                speedup_cell,
                *extra_cells,
                *error_cells,
            )
        )
        N_cell = ""
        error_cells = ["" for c in error_cells]

    clen = 7 if bluegene else 4
    return (r"\cline{2-" + str(clen) + "}").join(block_lines)


def make_tabular(Ns, Ps, *stats, bluegene=False):
    tabular_blocks = [make_header(bluegene=bluegene)]
    for N, *stat in zip(Ns, *stats):
        tabular_blocks.append(make_block(N, Ps, *stat, bluegene=bluegene))

    extra_cols = "r|r|r|r|r|r|" if bluegene else "r|r|r|"
    return (
        r"\begin{tabular}{|r|r|r|" + extra_cols + r"}"
        "\n"
        r"\hline" + r"\hline\hline".join(tabular_blocks) + r"\hline"
        "\n"
        r"\end{tabular}"
    )


def make_table(host, Ns, Ps, *stats):
    bluegene = host == "bluegene"
    table = (
        r"\caption{Результаты\\("
        f"{host.capitalize()}"
        ")}\n"
        r"\scriptsize"
        "\n" + make_tabular(Ns, Ps, *stats, bluegene=bluegene)
    )

    path = f"results_{host.lower()}/table.tex"
    with open(path, "w") as f:
        f.write(table)

    subprocess.check_output(["latexindent", f"--outputfile={path}", path])


def main():
    r = Results.load()
    rs = {
        "polus": r["H":"polus"],
        "bluegene": r["H":"bluegene", "O":0],
        "bluegene_openmp": r["H":"bluegene", "O":4],
    }

    Ns = r["N"]
    Ls = r["L"]
    Ps = {n: r["P"] for n, r in rs.items()}

    times = {}
    errors = {}
    for n, r in rs.items():
        times[n] = r.t(N=Ns, L=Ls, P=Ps[n])
        err = r.e(N=Ns, L=Ls, P=Ps[n])
        err = same_reduce(err, axes=(2, 3))
        err = err.max(axis=-1)
        errors[n] = err

    baselines = get_baselines(times, Ns, Ps)
    speedups = {n: (baselines[n][:, :, :1] / times[n]).max(axis=(1, 3)) for n in rs}
    baselines = {n: baselines[n].squeeze((1, 3)) for n in rs}

    make_table(
        "polus",
        Ns,
        Ps["polus"],
        baselines["polus"],
        speedups["polus"],
        errors["polus"],
    )

    make_table(
        "bluegene",
        Ns,
        Ps["bluegene"],
        baselines["bluegene"],
        speedups["bluegene"],
        baselines["bluegene_openmp"],
        speedups["bluegene_openmp"],
        errors["polus"],
    )


if __name__ == "__main__":
    main()
