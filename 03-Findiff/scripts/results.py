import pathlib

import numpy as np

R = 10  # number of trials
K = 20 + 1  # number of time steps
T = 0.01  # time bound

Rs = list(range(R))
Ks = list(range(K))


def _read_result(path):
    host = path.parent.name.split("_", 1)[-1]
    with path.open() as f:
        lines = [
            line
            for line in f.readlines()
            if "__PARAMS__" in line or "__TRIALS__" in line
        ]

    module = {"__TRIALS__": []}
    exec("\n".join(lines), module)
    params = module["__PARAMS__"]
    trials = module["__TRIALS__"]

    times = []
    errors = []
    for time, error in trials:
        times.append(time)
        errors.append(error)

    times = np.array(times)
    errors = np.array(errors)

    # Process params
    params["H"] = host
    assert params["K"] == K - 1, params
    assert params["T"] == T, params
    params.pop("K")
    params.pop("T")
    params["L"] = {1.0: "1", 3.141593: r"\pi"}[params["L"]]

    return params, times, errors


def same_reduce(vals, axes):
    keep = tuple(ax for ax in range(vals.ndim) if ax not in axes)
    shape = tuple(vals.shape[ax] for ax in keep) + (-1,)

    vals = vals.transpose(keep + axes)
    vals = vals.reshape(shape)
    if vals.shape[-1] == 1:
        return vals.squeeze(-1)

    diff = np.diff(vals, axis=-1)
    assert np.all(diff == 0), diff
    return vals[..., 0]


def get_baselines(times, Ns, Ps):
    baselines = {n: np.min(times[n], axis=(1, 3), keepdims=True) for n in times}
    for n in baselines:
        if "bluegene" not in n:
            continue

        # 512 nodes don't fit into the RAM of a single process
        # Estimate the baseline timings based on known speedup values
        b = baselines[n][Ns.index(512)]

        t1 = b[:, 1]
        t2 = b[:, -1]
        p0 = Ps[n][0]
        p1 = Ps[n][1]
        p2 = Ps[n][-1]

        alpha = np.log(t1 / t2) / np.log(p2 / p1)
        t0 = t1 * (p1 / p0) ** alpha

        b[:, 0] = t0

    return baselines


class Results:
    order = "HPOLN"
    time_shape = (R,)
    error_shape = (R, K)

    def __init__(self, files, keys, times, errors):
        lens = (len(files), len(keys), len(times), len(errors))
        assert len(set(lens)) == 1, lens

        self.files = files
        self.keys = keys
        self.times = times
        self.errors = errors

    @classmethod
    def load(cls, glob="results_*/*.out"):
        files = sorted(pathlib.Path().glob(glob))
        size = (len(files),)

        keys = []
        times = np.full(size + cls.time_shape, np.nan)
        errors = np.full(size + cls.error_shape, np.nan)

        for idx, file in enumerate(files):
            try:
                k, t, e = _read_result(file)
                keys.append(tuple(k[e] for e in cls.order))
                times[idx] = t
                errors[idx] = e
            except Exception as ex:
                raise RuntimeError(file) from ex

        reorder = sorted(range(len(keys)), key=lambda idx: keys[idx])
        files = [files[idx] for idx in reorder]
        keys = [keys[idx] for idx in reorder]
        times = times[reorder]
        errors = errors[reorder]

        return cls(files, keys, times, errors)

    def t(self, **kwargs):
        if kwargs:
            k = next(iter(kwargs))
            vs = kwargs.pop(k)
            return np.array([self[k:v].t(**kwargs) for v in vs])
        if self.times.shape[0] != 1:
            raise RuntimeError(self, self.times.shape)
        return self.times.squeeze(0).copy()

    def e(self, **kwargs):
        if kwargs:
            k = next(iter(kwargs))
            vs = kwargs.pop(k)
            return np.array([self[k:v].e(**kwargs) for v in vs])
        if self.errors.shape[0] != 1:
            raise RuntimeError(self, self.errors.shape)
        return self.errors.squeeze(0).copy()

    def dim_to_idx(self, dim):
        try:
            return self.order.index(dim)
        except ValueError:
            raise KeyError(dim)

    def __repr__(self):
        lens = [0 for _ in self.order]
        for key in self.keys:
            for idx, k in enumerate(key):
                lens[idx] = max(lens[idx], len(str(k)))

        singles = [(d, self[d]) for d in self.order]
        singles = [(d, *v) for d, v in singles if len(v) == 1]
        head = []
        for d, v in singles:
            head.append(f"{d}={v}")
        head = ", ".join(head)
        singles = {self.dim_to_idx(d) for d, _ in singles}

        spread = []
        for idx, key in enumerate(self.keys):
            line = []
            for jdx, k in enumerate(key):
                if jdx in singles:
                    continue
                line.append(f"{self.order[jdx]}={k:<{lens[jdx]}}")
            line = ", ".join(line)
            spread.append(f"    {line} ({self.files[idx]})\n")

        cname = self.__class__.__name__
        return f"{cname}({head}):\n" + ("".join(spread))

    def __len__(self):
        return len(self.keys)

    def __getitem__(self, index):
        if isinstance(index, slice):
            index = (index,)
        elif not isinstance(index, tuple):
            return self.get_keys(index)

        if any(isinstance(sl, slice) for sl in index):
            assert all(isinstance(sl, slice) for sl in index), index
            assert all(sl.step is None for sl in index), index
            assert len(index) == len(set(sl.start for sl in index)), index
            return self.get_filt(**{sl.start: sl.stop for sl in index})

    def get_keys(self, dim):
        dim = self.dim_to_idx(dim)

        keys = set()
        for k in self.keys:
            keys.add(k[dim])

        return sorted(keys)

    def get_filt(self, **params):
        filt = [(self.dim_to_idx(k), v) for k, v in params.items()]

        keep = []
        for idx, key in enumerate(self.keys):
            for dim, val in filt:
                if key[dim] != val:
                    break
            else:
                keep.append(idx)

        if not keep:
            raise RuntimeError(self, params)

        files = [self.files[idx] for idx in keep]
        keys = [self.keys[idx] for idx in keep]
        return self.__class__(files, keys, self.times[keep], self.errors[keep])
