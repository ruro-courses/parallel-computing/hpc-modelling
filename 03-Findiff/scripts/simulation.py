import numpy as np
import tqdm

from .grid import ND4, Grid


class SimulationCallback:
    def step_callback(self, g, step, u_comp):
        pass

    def final_callback(self, g, step, u_comp):
        pass


def get_dims(L=1, N=128, T=0.2, K=40):
    return {
        "L": ND4(x=L, y=L, z=L, t=T),
        "N": ND4(x=N, y=N, z=N, t=K),
    }


def run_simulation(callback, **kwargs):
    g = Grid(**get_dims(**kwargs))
    callback.step_callback(g, 0, g.u0)
    callback.step_callback(g, 1, g.u1)

    for step in tqdm.trange(2, g.N.t + 1):
        u_comp = g.u_step()
        callback.step_callback(g, step, u_comp)

    callback.final_callback(g, step, u_comp)
