import numpy as np

from .simulation import SimulationCallback, run_simulation


class PrintErrorHistory(SimulationCallback):
    def __init__(self):
        self.steps = []
        self.errors = []

    def step_callback(self, g, step, u_comp):
        self.steps.append(step * g.h.t)

        u_anal = g.u_analytical(step)
        error = abs(u_comp - u_anal).max()
        self.errors.append(error)

    def final_callback(self, g, step, u_comp):
        print(f"{g.N=}")
        print(f"{g.L=}")
        print(f"{g.h=}")
        print(f"{g.A=}")
        print(f"{g.B=}")
        for err in self.errors:
            print(f"{err:.6e}")
        print()


if __name__ == "__main__":
    run_simulation(PrintErrorHistory())
