import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

from .results import Ks, Results, same_reduce

mpl.rcParams["text.usetex"] = True


def plot_vs_errors(err, name, As, Bs, Cs, Cn):
    fig = plt.figure(figsize=(4.6, 4.6), tight_layout=True, dpi=400)
    ax = fig.add_subplot()

    for adx, A in enumerate(As):
        for bdx, B in enumerate(Bs):
            dCs = (max(Cs) - min(Cs)) / 40
            ax.set_xlim(min(Cs) - dCs, max(Cs) + dCs)
            ax.set_xticks(Cs)
            ax.set_xlabel(Cn)
            ax.set_ylabel("Error, $\delta$")

            vs = err[adx, bdx]
            color = f"C{adx}"
            style = ["-", "--"][bdx]
            edges = ["k", color][bdx]
            faces = [color, "w"][bdx]
            ax.plot(
                Cs,
                vs,
                color=color,
                linestyle=style,
                zorder=10,
                label=f"${A},\,{B}$",
            )
            ax.scatter(
                Cs,
                vs,
                s=5,
                edgecolors=edges,
                facecolors=faces,
                zorder=100,
            )

    ax.legend()
    fig.savefig(f"images/{name}_vs_errors.png", pad_inches=0.0, bbox_inches="tight")


def main():
    r = Results.load()
    Ns = r["N"]
    Ls = r["L"]

    r_b = r["H":"bluegene"]
    r_p = r["H":"polus"]

    Ps_b = r_b["P"]
    Ps_p = r_p["P"]

    Os_b = r_b["O"]
    Os_p = r_p["O"]

    err_b = same_reduce(r_b.e(N=Ns, L=Ls, P=Ps_b, O=Os_b), axes=(2, 3, 4))
    err_p = same_reduce(r_p.e(N=Ns, L=Ls, P=Ps_p, O=Os_p), axes=(2, 3, 4))
    assert np.all(err_b == err_p)
    err = err_p

    Ln = [f"L={l}" for l in Ls]
    Nn = [f"N={n}" for n in Ns]
    Kn = [r"\max_{t}"]
    plot_vs_errors(err, "time", Nn, Ln, Ks, "Time step, $t$")

    err = err.max(axis=-1, keepdims=True)
    err = err.transpose(1, 2, 0)
    plot_vs_errors(err, "size", Ln, Kn, Ns, "Grid size, $N$")


if __name__ == "__main__":
    main()
