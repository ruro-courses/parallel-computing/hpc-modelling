import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

from .results import R, Results, get_baselines

mpl.rcParams["text.usetex"] = True


def plot_speedup(speedup, name, Ns, Ls, Ps):
    fig = plt.figure(figsize=(4.6, 4.6), tight_layout=True, dpi=400)
    ax = fig.add_subplot()

    max_speedup = np.max(speedup, axis=(1, 3))
    pts = R * len(Ls)

    ax.plot(Ps, Ps, color="k", label="Best Theoretical Speedup")
    for ndx, N in enumerate(Ns):
        dPs = (max(Ps) - min(Ps)) / 40
        ax.set_xlim(min(Ps) - dPs, max(Ps) + dPs)
        ax.set_ylim(min(Ps) - dPs, max(Ps) + dPs)
        ax.set_xticks(Ps)
        ax.set_xlabel("Num. Processes, $P$")
        ax.set_ylabel("Relative Speedup, $S$")

        color = f"C{ndx}"
        style = "-"
        edges = "k"
        faces = color

        ax.scatter(
            np.tile([[p] for p in Ps], pts),
            speedup[ndx].transpose(1, 0, 2).reshape(-1, pts),
            s=5,
            linewidth=0.25,
            edgecolors=edges,
            facecolors=faces,
            zorder=100,
        )
        ax.plot(
            Ps,
            max_speedup[ndx],
            color=color,
            linestyle=style,
            zorder=10,
            label=f"$N={N}$",
        )

    ax.legend()
    fig.savefig(f"images/{name}_speedup.png", pad_inches=0.0, bbox_inches="tight")


def main():
    r = Results.load()
    rs = {
        "polus": r["H":"polus"],
        "bluegene": r["H":"bluegene", "O":0],
        "bluegene_openmp": r["H":"bluegene", "O":4],
    }

    Ns = r["N"]
    Ls = r["L"]
    Ps = {n: r["P"] for n, r in rs.items()}
    times = {n: r.t(N=Ns, L=Ls, P=Ps[n]) for n, r in rs.items()}
    baselines = get_baselines(times, Ns, Ps)
    speedups = {n: baselines[n][:, :, :1] / times[n] for n in rs}

    for name in rs:
        plot_speedup(speedups[name], name, Ns, Ls, Ps[name])


if __name__ == "__main__":
    main()
