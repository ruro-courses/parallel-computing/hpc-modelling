import glob
import os

import ffmpeg
import tqdm

from .simulation import SimulationCallback, get_dims, run_simulation
from .simulation_slices import plot_volume_slices


class AnimateSlices(SimulationCallback):
    def __init__(self, kind, vmax=0, skip=8, dpi=200):
        self.kind = kind
        self.skip = skip
        self.dpi = dpi
        self.vmax = vmax

    def step_callback(self, g, step, u_comp):
        if step % self.skip:
            return

        if self.kind == "comp":
            data = u_comp
        else:
            u_anal = g.u_analytical(step)
            if self.kind == "anal":
                data = u_anal
            elif self.kind == "error":
                data = abs(u_comp - u_anal)
            else:
                raise NotImplementedError(f"Unknown kind '{kind}'")

        self.vmax = max(self.vmax, abs(data).max())
        frame = step // self.skip
        plot_volume_slices(
            g,
            f"_ffmpeg{frame}",
            data,
            vmax=self.vmax,
            dpi=self.dpi,
            symmetric=self.kind != "error",
        )

    def final_callback(self, g, step, u_comp):
        (
            ffmpeg.input(f"images/_ffmpeg%d_slice_{g.L=}_{g.N=}.png")
            .output(f"images/{self.kind}_slice_{g.L=}_{g.N=}.mp4")
            .overwrite_output()
        ).run()

        for f in glob.glob(f"images/_ffmpeg*"):
            os.unlink(f)


def stack_videos(kinds, **kwargs):
    dims = get_dims(**kwargs)
    L = dims["L"]
    N = dims["N"]

    paths = [f"images/{kind}_slice_g.{L=}_g.{N=}.mp4" for kind in kinds]
    shapes = []
    for path in paths:
        (stream,) = ffmpeg.probe(path)["streams"]
        shapes.append((stream["width"], stream["height"]))

    tw = max(w for w, _ in shapes)

    vids = []
    for path, shape in zip(paths, shapes):
        _, sh = shape

        vids.append(
            ffmpeg.input(path).filter_("pad", h=sh, w=tw, x=0, y=0, color="white")
        )

    (
        ffmpeg.filter(vids, "vstack")
        .output(f"images/stacked_slice_g.{L=}_g.{N=}.mp4")
        .overwrite_output()
    ).run()


if __name__ == "__main__":
    run_simulation(AnimateSlices("error", vmax=2.5e-04), T=0.8, K=256)
    run_simulation(AnimateSlices("comp"), T=0.8, K=256)
    stack_videos(["comp", "error"], T=0.8, K=256)
