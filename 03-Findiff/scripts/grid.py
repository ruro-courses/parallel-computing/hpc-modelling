import typing

import numpy as np

from .laplace import laplace_3d


class ND4(typing.NamedTuple):
    x: typing.Union[int, float]
    y: typing.Union[int, float]
    z: typing.Union[int, float]
    t: typing.Union[int, float]


class Grid:
    def __init__(self, L, N):
        # Grid parameters
        self.L = L
        self.N = N
        self.h = h = ND4(L.x / N.x, L.y / N.y, L.z / N.z, L.t / N.t)

        # Analytical solution parameters
        self.periodic = periodic = (True, False, False)
        self.A = ND4(
            2 * np.pi / L.x,
            np.pi / L.y,
            np.pi / L.z,
            np.pi * np.sqrt(4 / (L.x * L.x) + 1 / (L.y * L.y) + 1 / (L.z * L.z)),
        )
        self.B = ND4(0, 0, 0, 2 * np.pi + np.pi / 2)  # np.pi/2 changes sin -> cos

        # Initialize u0 and u1
        self.points = points = np.mgrid[
            0 : L.x - h.x : 1j * N.x,
            0 : L.y - h.y : 1j * N.y,
            0 : L.z - h.z : 1j * N.z,
            0:0:1j,
        ].squeeze(axis=-1)

        self._state = np.zeros((N.x, N.y, N.z, 2))
        self.u0[:] = self.u_analytical()

        u0 = self.u0
        coeff = h.t * h.t / 2
        lap = laplace_3d(u0, h, periodic)
        self.u1[:] = u0 + coeff * lap

    @property
    def u0(self):
        return self._state[..., 0]

    @property
    def u1(self):
        return self._state[..., 1]

    def u_analytical(self, step=None):
        p = self.points
        if step is not None:
            p[-1] = step * self.h.t
        p = p.transpose(1, 2, 3, 0)

        return np.sin(self.A * p + self.B).prod(axis=-1)

    def u_step(self):
        # Step in time
        u1 = self.u1
        coef = self.h.t * self.h.t
        lap = laplace_3d(u1, self.h, self.periodic)
        u = coef * lap + 2 * u1 - self.u0

        # Update state
        self.u0[:] = self.u1
        self.u1[:] = u

        return u
