import numpy as np


def laplace(u, h, periodic, axis):
    u = u.swapaxes(0, axis)

    # enforce periodic boundary condition
    u_ext = np.pad(
        u,
        ((1, 1),) + (u.ndim - 1) * ((0, 0),),
        mode="wrap" if periodic else "constant",
    )

    # extract previous and next
    u_p = u_ext[:-2]
    u_n = u_ext[2:]

    # compute laplace operator (inplace)
    u = (u_n - 2 * u + u_p) / (h * h)

    # enforce first-type boundary condition
    if not periodic:
        u[0] = 0

    u = u.swapaxes(0, axis)
    return u


def laplace_3d(u, h, p):
    s = np.zeros_like(u)
    for h_, periodic, axis in zip(h, p, range(3)):
        s += laplace(u, h_, periodic, axis)
    return s
