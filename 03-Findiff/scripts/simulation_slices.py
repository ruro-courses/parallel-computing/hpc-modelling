import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

from .simulation import SimulationCallback, run_simulation

mpl.rcParams["text.usetex"] = True


def plot_volume_slices(g, name, vol, vmax, dpi=200, nrows=1, ncols=8, symmetric=True):
    if symmetric:
        cmap = mpl.colors.LinearSegmentedColormap.from_list("C0kC3", ["C0", "k", "C3"])
        norm = mpl.colors.TwoSlopeNorm(vmin=-vmax, vcenter=0, vmax=vmax)
    else:
        cmap = mpl.colors.LinearSegmentedColormap.from_list("kC3", ["k", "C3"])
        norm = mpl.colors.Normalize(vmin=0, vmax=vmax)

    fig, axes = plt.subplots(
        figsize=(12.8, 4.8),
        dpi=dpi,
        nrows=3 * nrows,
        ncols=ncols,
    )
    fig.subplots_adjust(
        top=1.0,
        right=1.0,
        bottom=0.0,
        left=0.0,
        hspace=0.0,
        wspace=0.02,
    )
    num_axes = nrows * ncols
    for dim in range(3):
        dim_axes = list(axes.flat)[dim * num_axes : (dim + 1) * num_axes]
        for idx, ax in enumerate(dim_axes):
            idx = 2 * idx + 1
            idx = (idx * g.N[dim]) // (2 * num_axes)
            pos = idx * g.h[dim]
            slice_ = vol[idx]
            cbar = ax.imshow(slice_, interpolation="nearest", cmap=cmap, norm=norm)
            ax.set_title(f"${'xyz'[dim]} = {pos:.4f}$")
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)
        vol = vol.transpose(1, 2, 0)
    fig.colorbar(cbar, ax=axes.ravel().tolist(), pad=0.02)
    fig.savefig(
        f"images/{name}_slice_{g.L=}_{g.N=}.png",
        bbox_inches="tight",
    )
    plt.close(fig)


class PlotLastSlice(SimulationCallback):
    def final_callback(self, g, step, u_comp):
        u_anal = g.u_analytical(step)

        diff = abs(u_comp - u_anal)
        plot_volume_slices(g, "error", diff, vmax=diff.max(), symmetric=False)

        vmax = max(abs(u_anal).max(), abs(u_comp).max())
        plot_volume_slices(g, "comp", u_comp, vmax=vmax)
        plot_volume_slices(g, "anal", u_anal, vmax=vmax)


if __name__ == "__main__":
    run_simulation(PlotLastSlice(), T=0.3, K=64)
